# Luke's config for the Zoomer Shell
source "`ueberzug library`"
# Enable colors and change prompt:
autoload -U colors && colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
[ -n "$NNNLVL" ] && PS1="N$NNNLVL $PS1"
# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history
setopt autocd
# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots) # Include hidden files.
# vi mode
bindkey -v
export KEYTIMEOUT=1
# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char
# Change cursor shape for different vi modes.
function zle-keymap-select {
if [[ ${KEYMAP} == vicmd ]] ||
[[ $1 = 'block' ]]; then
echo -ne '\e[1 q'
elif [[ ${KEYMAP} == main ]] ||
[[ ${KEYMAP} == viins ]] ||
[[ ${KEYMAP} = '' ]] ||
[[ $1 = 'beam' ]]; then
echo -ne '\e[5 q'
fi
}
zle -N zle-keymap-select
zle-line-init() {
zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.
export PATH="$PATH:/home/paradox/depot_tools"
	# Use lf to switch directories and bind it to ctrl-o
lfcd () {
tmp="$(mktemp)"
lf -last-dir-path="$tmp" "$@"
if [ -f "$tmp" ]; then
dir="$(cat "$tmp")"
rm -f "$tmp"
[ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
fi
}
if [ "$TERM" != "linux" ] && [ "$TERM" != "vt100" ] && [ "$TERM" != "vt220" ]; then

  printf "\033]4;236;rgb:32/30/2f\033\\"
  printf "\033]4;234;rgb:1d/20/21\033\\"

  printf "\033]4;235;rgb:28/28/28\033\\"
  printf "\033]4;237;rgb:3c/38/36\033\\"
  printf "\033]4;239;rgb:50/49/45\033\\"
  printf "\033]4;241;rgb:66/5c/54\033\\"
  printf "\033]4;243;rgb:7c/6f/64\033\\"

  printf "\033]4;244;rgb:92/83/74\033\\"
  printf "\033]4;245;rgb:92/83/74\033\\"

  printf "\033]4;228;rgb:f2/e5/bc\033\\"
  printf "\033]4;230;rgb:f9/f5/d7\033\\"

  printf "\033]4;229;rgb:fb/f1/c7\033\\"
  printf "\033]4;223;rgb:eb/db/b2\033\\"
  printf "\033]4;250;rgb:d5/c4/a1\033\\"
  printf "\033]4;248;rgb:bd/ae/93\033\\"
  printf "\033]4;246;rgb:a8/99/84\033\\"

  printf "\033]4;167;rgb:fb/49/34\033\\"
  printf "\033]4;142;rgb:b8/bb/26\033\\"
  printf "\033]4;214;rgb:fa/bd/2f\033\\"
  printf "\033]4;109;rgb:83/a5/98\033\\"
  printf "\033]4;175;rgb:d3/86/9b\033\\"
  printf "\033]4;108;rgb:8e/c0/7c\033\\"
  printf "\033]4;208;rgb:fe/80/19\033\\"

  printf "\033]4;88;rgb:9d/00/06\033\\"
  printf "\033]4;100;rgb:79/74/0e\033\\"
  printf "\033]4;136;rgb:b5/76/14\033\\"
  printf "\033]4;24;rgb:07/66/78\033\\"
  printf "\033]4;96;rgb:8f/3f/71\033\\"
  printf "\033]4;66;rgb:42/7b/58\033\\"
  printf "\033]4;130;rgb:af/3a/03\033\\"
fi
bindkey -s '^o' 'lfcd\n'
function yta() {
    mplayer ytdl://ytsearch:"$*"
}
# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line
alias n2="dvtm -m '\\\' nnn nnn"
alias dir="dir -A --color  --group-directories-first"
alias shell="adb shell"
#alias mplayer="mplayer -vo fbdev2 -fs -zoom -xy 1366"
alias mplayer="mplayer -vo fbdev2 -vf scale=1366:768"
alias mp2="mplayer -vo fbdev2 -vf scale=683:384 -geometry 725:0"
alias cp="cpg -g"
alias mv="mvg -g"
alias data="cd /home/paradox/.local/media/data"
alias mnt="cd /home/paradox/.local/media/mnt"
alias src="cd /home/paradox/.local/src"
alias speedtest="speedtest --server 14238"
alias mount="sudo mount"
alias umount="sudo umount"
alias make="make -j8"
#alias shutdown="sudo poweroff"
#alias reboot="sudo reboot"
# Load aliases and shortcuts if existent.
[ -f "$HOME/.config/shortcutrc" ] && source "$HOME/.config/shortcutrc"
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"

source /home/paradox/.local/share/zsh/colored-man-pages_mod.plugin.zsh
# Load zsh-syntax-highlighting; should be last.
source /home/paradox/.local/lib/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
