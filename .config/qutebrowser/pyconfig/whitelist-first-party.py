
from qutebrowser.api import interceptor


# Must load as late as possible to do anything.

def intercept(info):
	if (info.first_party_url is not None and
		info.first_party_url.isValid() and
		info.first_party_url == info.request_url):

		info.is_blocked = False

interceptor.register(intercept)
