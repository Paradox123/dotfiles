from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401
from qutebrowser.config.config import ConfigContainer # noqa: F401

config = config  # type: ConfigAPI # noqa: F821 pylint: disable=E0602,C0103
c = c  # type: ConfigContainer # noqa: F821 pylint: disable=E0602,C0103

config.bind(
	'<f11>',
	'config-cycle -t statusbar.hide ;; config-cycle -t tabs.show always switching ;; fullscreen',
	mode='normal')
