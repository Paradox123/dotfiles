from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401
from qutebrowser.config.config import ConfigContainer # noqa: F401

config = config  # type: ConfigAPI # noqa: F821 pylint: disable=E0602,C0103
c = c  # type: ConfigContainer # noqa: F821 pylint: disable=E0602,C0103

BUNNY_URL="https://our.intern.facebook.com/intern/bunny/?q={}"

c.url.searchengines['bunny'] = BUNNY_URL
c.url.searchengines['b'] = BUNNY_URL

PASSTHROUGH_TERM = [
	'i', 's', 'sw', 'w', 'tbgs', 'fbgs', 'fbgf', 'zbgs', 'zbgf', 'abgs', 'codex', '@od', 'od',
	'@sb', '@intern', 'sb', 'tw', 'fbpkg', 'smc', 'alitepath', 'duf', 'wut', 'sf']
for t in PASSTHROUGH_TERM:
	c.url.searchengines[t] = BUNNY_URL.format(t + " {}")
