// ==UserScript==
// @name          Dark HN
// @namespace     http://userstyles.org
// @description	  A dark, readable HackerNews
// @author        Spencer Williams
// @homepage      https://userstyles.org/styles/127454
// @include       about:blank
// @include       https://about:blank
// @include       http://about:blank
// @run-at        document-start
// @version       0.20160504115313
// ==/UserScript==
(function() {var css = [
	"body{background-color: rgb(46,54,69) !important"
].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();
