// ==UserScript==
// @name          Washingtonpost Remove subscribe pop-up
// @namespace     http://userstyles.org
// @description	  Remove subscribe popup on Washingtonpost.com
// @author        _A7med
// @homepage      https://userstyles.org/styles/146590
// @include       http://washingtonpost.com/*
// @include       https://washingtonpost.com/*
// @include       http://*.washingtonpost.com/*
// @include       https://*.washingtonpost.com/*
// @run-at        document-start
// @version       0.20190530191833
// ==/UserScript==
(function() {var css = [
	"html {",
	"     height: auto !important;",
	"     overflow-y: auto !important;",
	" }",
	" .jqm-init,",
	" .wp_signin,",
	" #wp_signin,",
	" #intOffer,",
	" .translucentBg,",
	" #drawbridge-root {",
	"     display: none !important;",
	" }",
	"",
	"div[style=\"display: block;\"] {",
	"    display: none !important;",
	"} ",
	"",
	"div[id=\"i_userMessages\"]{",
	"    display: none !important;",
	"}"
].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();
