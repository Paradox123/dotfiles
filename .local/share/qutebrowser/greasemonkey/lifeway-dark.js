// ==UserScript==
// @name          Suckless Dark Breeze
// @namespace     http://userstyles.org
// @description	  Dark theme for Suckless.org, made to blend nicely with breeze dark (KDE theme)
// @author        Kajika
// @homepage      https://userstyles.org/styles/168625
// @include       http://lifeway.com/*
// @include       https://lifeway.com/*
// @include       http://*.lifeway.com/*
// @include       https://*.lifeway.com/*
// @run-at        document-start
// @version       0.20190206082650
// ==/UserScript==
(function() {var css = [
    "input#global-search__input.global-search__input.form-control{ background: #333 !important; border-color: #333 !important;}",
    "div.head-nav__container.container-fluid.inner-max{ background: #222 !important;}",
    "nav{background: #222 !important;}",
    "nav.nav{background: #333 !important;}",
    "div.author-block.card.card-block.card--light{background: #333 !important; border-color: #333 !important; color: #eee !important;}",
    "div.body-content{background: #222 !important;color: #eee !important;}",
    "h1{color: #eee !important;}",
    "input{background: #444 !important;border-color: #444 !important;}",
    "body{background: #222 !important;color: #eee !important;}"
].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();
