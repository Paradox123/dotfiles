// ==UserScript==
// @name          NYTimes Bypass Lock
// @namespace     http://userstyles.org
// @description	  Bypass NYTimes CSS lockdown.
// @author        jwarren
// @homepage      https://userstyles.org/styles/180800
// @include       http://nytimes.com/*
// @include       https://nytimes.com/*
// @include       http://*.nytimes.com/*
// @include       https://*.nytimes.com/*
// @run-at        document-start
// @version       0.20200305172430
// ==/UserScript==
(function() {var css = [
	"body {",
	"    overflow-x: initial;",
	"}",
	"",
	"#gateway-content {",
	"    display: none;",
	"}",
	"",
	"#app>div>div,",
	"#site-content {",
	"    overflow: initial !important;",
	"    position: relative !important;",
	"}",
	"",
	"#app>div>div>div:last-child:empty {",
	"    display: none;",
	"}"
].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();
