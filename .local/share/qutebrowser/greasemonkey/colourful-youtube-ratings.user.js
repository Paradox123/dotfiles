// ==UserScript==
// @name          Colourful YouTube Ratings
// @namespace     http://userstyles.org
// @description	  Gives colour to the YouTube rating bar and like/dislike buttons.
// @author        InvoxiPlayGames
// @homepage      https://userstyles.org/styles/167450
// @include       http://www.youtube.com/*
// @include       https://www.youtube.com/*
// @include       http://*.www.youtube.com/*
// @include       https://*.www.youtube.com/*
// @include       http://youtube.com/*
// @include       https://youtube.com/*
// @include       http://*.youtube.com/*
// @include       https://*.youtube.com/*
// @run-at        document-start
// @version       0.20190104070521
// ==/UserScript==
(function() {var css = [
	"#like-bar.ytd-sentiment-bar-renderer { background: #009900!important; } /* Like bar */",
	"#container.ytd-sentiment-bar-renderer { background: #FF2929!important; } /* Dislike bar */",
	"ytd-menu-renderer ytd-toggle-button-renderer.style-default-active[is-icon-button]:nth-of-type(1) { color: #009900; } /* Liked button */",
	"ytd-menu-renderer ytd-toggle-button-renderer.style-default-active[is-icon-button]:nth-of-type(2) { color: #FF2929; } /* Disliked button */"
].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();
