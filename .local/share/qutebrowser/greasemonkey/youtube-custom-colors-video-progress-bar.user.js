// ==UserScript==
// @name          YouTube - Custom colors video progress bar
// @namespace     http://userstyles.org
// @description	  Brighten up your YouTube experience with this custom video progress bar. The subtle yet vibrant glows will bring some life and color to the YouTube HTML5 video player.
// @author        zoid
// @homepage      https://userstyles.org/styles/95280
// @include       http://youtube.com/*
// @include       https://youtube.com/*
// @include       http://*.youtube.com/*
// @include       https://*.youtube.com/*
// @run-at        document-start
// @version       0.20170930175248
// ==/UserScript==
(function() {var css = [
	"/* scrubber button */",
	".html5-scrubber-button:hover, .ytp-scrubber-button:hover, .ytp-swatch-background-color, .ytp-swatch-background-color-secondary {",
	"   background: #2793E6 !important;",
	"}",
	"/* progress bar */",
	".html5-play-progress, .ytp-play-progress {",
	"   background: #2793E6 !important; ",
	"}",
	".ytp-volume-slider-track, .ytp-volume-slider-handle:before {",
	"   background: #2793E6 !important;",
	"   z-index: -117;",
	"}",
	".ytp-settings-button.ytp-hd-quality-badge::after, .ytp-settings-button.ytp-4k-quality-badge::after, .ytp-settings-button.ytp-5k-quality-badge::after, .ytp-settings-button.ytp-8k-quality-badge::after, .ytp-settings-button.ytp-3d-badge::after {",
	"   background-color: #2793E6 !important;",
	"}",
	".ytp-swatch-color {",
	"   color: #2793E6 !important;",
	"}",
	".ytp-menuitem[aria-checked=\"true\"] .ytp-menuitem-toggle-checkbox {",
	"   background: #2793E6 !important;",
	"}",
	".ytp-chrome-controls .ytp-button.ytp-youtube-button:hover:not([aria-disabled=\"true\"]):not([disabled]) .ytp-svg-fill-logo-tube-lozenge {",
	"   fill: #2793E6 !important;",
	"}",
	".ytp-cued-thumbnail-overlay:hover .ytp-large-play-button-bg, .ytp-large-play-button.ytp-touch-device .ytp-large-play-button-bg {",
	"   fill: #2793E6 !important;",
	"}",
	".resume-playback-progress-bar {",
	"   background: #2793E6 !important;",
	"}",
	".ytp-chrome-controls .ytp-button[aria-pressed]::after {",
	"   background-color: #2793E6 !important;",
	"}",
	".yt-uix-checkbox-on-off input-extras2-disabled[type=\"checkbox\"]:checked + label {",
	"   background-color: #2793E6 !important;",
	"}",
	".video-extras-sparkbar-likes-extras2-disabled {",
	"   background-color: #2793E6 !important;",
	"}"
].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();
