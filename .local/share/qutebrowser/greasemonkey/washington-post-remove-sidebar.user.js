// ==UserScript==
// @name          Washington Post - Remove Sidebar
// @namespace     http://userstyles.org
// @description	  Remove right sided sidebar from washingtonpost.com
// @author        Mose250
// @homepage      https://userstyles.org/styles/45393
// @include       http://washingtonpost.com/*
// @include       https://washingtonpost.com/*
// @include       http://*.washingtonpost.com/*
// @include       https://*.washingtonpost.com/*
// @run-at        document-start
// @version       0.20110318201327
// ==/UserScript==
(function() {var css = [
	".wp-column.six.end { display: none !important; }",
	".wp-column.ten { width: 982 px !important; }"
].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();
