// ==UserScript==
// @name          Dark Youtube 2018
// @namespace     http://userstyles.org
// @description	  Dark version of the new 2018 Youtube layout.
// @author        agnostik_one
// @homepage      https://userstyles.org/styles/151445
// @include       http://invidious.snopyta.org/*
// @include       https://invidious.snopyta.org/*
// @include       http://*.invidious.snopyta.org/*
// @include       https://*.invidious.snopyta.org/*
// @include       http://invidio.us/*
// @include       https://invidio.us/*
// @include       http://*.invidio.us/*
// @include       https://*.invidio.us/*
// @include       http://invidious.toot.koeln/*
// @include       https://invidious.toot.koeln/*
// @include       http://*.invidious.toot.koeln/*
// @include       https://*.invidious.toot.koeln/*
// @include       http://invidious.ggc-project.de/*
// @include       https://invidious.ggc-project.de/*
// @include       http://*.invidious.ggc-project.de/*
// @include       https://*.invidious.ggc-project.de/*
// @include       http://invidious.13ad.de/*
// @include       https://invidious.13ad.de/*
// @include       http://*.invidious.13ad.de/*
// @include       https://*.invidious.13ad.de/*
// @run-at        document-start
// @version       0.20180927232040
// ==/UserScript==
(function() {var css = [
    "a:hover,",
"a:active {",
    "color: rgb(0, 182, 240) !important;",
"}",

"a {",
  "color: #a0a0a0 !important;",
  "text-decoration: none !important",
"}",

"body {",
  "background-color: rgba(35, 35, 35, 1) !important;",
  "color: #f0f0f0 !important;",
"}",

".pure-form legend {",
  "color: #f0f0f0 !important;",
"}",

".pure-menu-heading {",
  "color: #f0f0f0 !important;",
"}",

"input,",
"select,",
"textarea {",
  "color: rgba(35, 35, 35, 1) !important;",
"}",

"i {",
  "color: #f0f0f0 !important;",
"}",

"i:hover {",
  "color: rgb(0, 182, 240) !important;",
"}",

"b {",
  "color: #232323 !important;",
"}",

".navbar > .searchbar input {",
  "background-color: inherit !important;",
  "color: inherit !important;",
    "}",

].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();
