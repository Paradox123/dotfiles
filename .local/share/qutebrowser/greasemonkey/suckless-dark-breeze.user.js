// ==UserScript==
// @name          Suckless Dark Breeze
// @namespace     http://userstyles.org
// @description	  Dark theme for Suckless.org, made to blend nicely with breeze dark (KDE theme)
// @author        Kajika
// @homepage      https://userstyles.org/styles/168625
// @include       http://suckless.org/*
// @include       https://suckless.org/*
// @include       http://*.suckless.org/*
// @include       https://*.suckless.org/*
// @run-at        document-start
// @version       0.20190206082650
// ==/UserScript==
(function() {var css = [
	":root",
	"{",
	"    --main-bg-color: #21262b;",
	"	--main-fg-color: #bbb;",
	"	--lighter-bg-color: #31363b;",
	"    --lighter-fg-color: #ccc;",
	"	--highlight-bg-color: #41464b;",
	"    --highlight-fg-color: #ddd;",
	"}",
	"",
	"body, #header",
	"{",
	"    background-color: var(--main-bg-color);",
	"    color: var(--main-fg-color);",
	"}",
	"",
	"#menu a",
	"{",
	"    color: var(--lighter-fg-color);",
	"}",
	"",
	"#nav a:hover",
	"{",
	"    background-color: var(--highlight-bg-color);",
	"    color: var(--main-fg-color);",
	"}"
].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();
