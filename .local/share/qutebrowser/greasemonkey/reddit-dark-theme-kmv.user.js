// ==UserScript==
// @name          Reddit Dark Theme - KMV
// @namespace     http://userstyles.org
// @description	  Reddit Dark Theme - International Version.
// @author        KMVerum
// @homepage      https://userstyles.org/styles/174881
// @include       http://www.reddit.com/*
// @include       https://www.reddit.com/*
// @include       http://*.www.reddit.com/*
// @include       https://*.www.reddit.com/*
// @include       http://old.reddit.com/*
// @include       https://old.reddit.com/*
// @include       http://*.old.reddit.com/*
// @include       https://*.old.reddit.com/*
// @include       http://np.reddit.com/*
// @include       https://np.reddit.com/*
// @include       http://*.np.reddit.com/*
// @include       https://*.np.reddit.com/*
// @include       http://snew.notabug.io/*
// @include       https://snew.notabug.io/*
// @include       http://*.snew.notabug.io/*
// @include       https://*.snew.notabug.io/*
// @include       http://notabug.io/*
// @include       https://notabug.io/*
// @include       http://*.notabug.io/*
// @include       https://*.notabug.io/*
// @include       http://af.reddit.com/*
// @include       https://af.reddit.com/*
// @include       http://*.af.reddit.com/*
// @include       https://*.af.reddit.com/*
// @include       http://ar.reddit.com/*
// @include       https://ar.reddit.com/*
// @include       http://*.ar.reddit.com/*
// @include       https://*.ar.reddit.com/*
// @include       http://be.reddit.com/*
// @include       https://be.reddit.com/*
// @include       http://*.be.reddit.com/*
// @include       https://*.be.reddit.com/*
// @include       http://bg.reddit.com/*
// @include       https://bg.reddit.com/*
// @include       http://*.bg.reddit.com/*
// @include       https://*.bg.reddit.com/*
// @include       http://bn-IN.reddit.com/*
// @include       https://bn-IN.reddit.com/*
// @include       http://*.bn-IN.reddit.com/*
// @include       https://*.bn-IN.reddit.com/*
// @include       http://bn.bd.reddit.com/*
// @include       https://bn.bd.reddit.com/*
// @include       http://*.bn.bd.reddit.com/*
// @include       https://*.bn.bd.reddit.com/*
// @include       http://bs.reddit.comx/*
// @include       https://bs.reddit.comx/*
// @include       http://*.bs.reddit.comx/*
// @include       https://*.bs.reddit.comx/*
// @include       http://ca.reddit.com/*
// @include       https://ca.reddit.com/*
// @include       http://*.ca.reddit.com/*
// @include       https://*.ca.reddit.com/*
// @include       http://cs.reddit.com/*
// @include       https://cs.reddit.com/*
// @include       http://*.cs.reddit.com/*
// @include       https://*.cs.reddit.com/*
// @include       http://cy.reddit.com/*
// @include       https://cy.reddit.com/*
// @include       http://*.cy.reddit.com/*
// @include       https://*.cy.reddit.com/*
// @include       http://da.reddit.com/*
// @include       https://da.reddit.com/*
// @include       http://*.da.reddit.com/*
// @include       https://*.da.reddit.com/*
// @include       http://dd.reddit.com/*
// @include       https://dd.reddit.com/*
// @include       http://*.dd.reddit.com/*
// @include       https://*.dd.reddit.com/*
// @include       http://de.reddit.com/*
// @include       https://de.reddit.com/*
// @include       http://*.de.reddit.com/*
// @include       https://*.de.reddit.com/*
// @include       http://ds.reddit.com/*
// @include       https://ds.reddit.com/*
// @include       http://*.ds.reddit.com/*
// @include       https://*.ds.reddit.com/*
// @include       http://el.reddit.com/*
// @include       https://el.reddit.com/*
// @include       http://*.el.reddit.com/*
// @include       https://*.el.reddit.com/*
// @include       http://en.reddit.com/*
// @include       https://en.reddit.com/*
// @include       http://*.en.reddit.com/*
// @include       https://*.en.reddit.com/*
// @include       http://en.au.reddit.com/*
// @include       https://en.au.reddit.com/*
// @include       http://*.en.au.reddit.com/*
// @include       https://*.en.au.reddit.com/*
// @include       http://en.ca.reddit.com/*
// @include       https://en.ca.reddit.com/*
// @include       http://*.en.ca.reddit.com/*
// @include       https://*.en.ca.reddit.com/*
// @include       http://en.gb.reddit.com/*
// @include       https://en.gb.reddit.com/*
// @include       http://*.en.gb.reddit.com/*
// @include       https://*.en.gb.reddit.com/*
// @include       http://en.us.reddit.com/*
// @include       https://en.us.reddit.com/*
// @include       http://*.en.us.reddit.com/*
// @include       https://*.en.us.reddit.com/*
// @include       http://eo.reddit.com/*
// @include       https://eo.reddit.com/*
// @include       http://*.eo.reddit.com/*
// @include       https://*.eo.reddit.com/*
// @include       http://es.reddit.com/*
// @include       https://es.reddit.com/*
// @include       http://*.es.reddit.com/*
// @include       https://*.es.reddit.com/*
// @include       http://es-ar.reddit.com/*
// @include       https://es-ar.reddit.com/*
// @include       http://*.es-ar.reddit.com/*
// @include       https://*.es-ar.reddit.com/*
// @include       http://es-cl.reddit.com/*
// @include       https://es-cl.reddit.com/*
// @include       http://*.es-cl.reddit.com/*
// @include       https://*.es-cl.reddit.com/*
// @include       http://es-mx.reddit.com/*
// @include       https://es-mx.reddit.com/*
// @include       http://*.es-mx.reddit.com/*
// @include       https://*.es-mx.reddit.com/*
// @include       http://et.reddit.com/*
// @include       https://et.reddit.com/*
// @include       http://*.et.reddit.com/*
// @include       https://*.et.reddit.com/*
// @include       http://eu.reddit.com/*
// @include       https://eu.reddit.com/*
// @include       http://*.eu.reddit.com/*
// @include       https://*.eu.reddit.com/*
// @include       http://fa.reddit.com/*
// @include       https://fa.reddit.com/*
// @include       http://*.fa.reddit.com/*
// @include       https://*.fa.reddit.com/*
// @include       http://fi.reddit.com/*
// @include       https://fi.reddit.com/*
// @include       http://*.fi.reddit.com/*
// @include       https://*.fi.reddit.com/*
// @include       http://fil.reddit.com/*
// @include       https://fil.reddit.com/*
// @include       http://*.fil.reddit.com/*
// @include       https://*.fil.reddit.com/*
// @include       http://fr.reddit.com.reddit.com/*
// @include       https://fr.reddit.com.reddit.com/*
// @include       http://*.fr.reddit.com.reddit.com/*
// @include       https://*.fr.reddit.com.reddit.com/*
// @include       http://fr.ca.reddit.com/*
// @include       https://fr.ca.reddit.com/*
// @include       http://*.fr.ca.reddit.com/*
// @include       https://*.fr.ca.reddit.com/*
// @include       http://fy-NL.reddit.com/*
// @include       https://fy-NL.reddit.com/*
// @include       http://*.fy-NL.reddit.com/*
// @include       https://*.fy-NL.reddit.com/*
// @include       http://ga-ie.reddit.com/*
// @include       https://ga-ie.reddit.com/*
// @include       http://*.ga-ie.reddit.com/*
// @include       https://*.ga-ie.reddit.com/*
// @include       http://gd.reddit.com/*
// @include       https://gd.reddit.com/*
// @include       http://*.gd.reddit.com/*
// @include       https://*.gd.reddit.com/*
// @include       http://gl.reddit.com/*
// @include       https://gl.reddit.com/*
// @include       http://*.gl.reddit.com/*
// @include       https://*.gl.reddit.com/*
// @include       http://he.reddit.com/*
// @include       https://he.reddit.com/*
// @include       http://*.he.reddit.com/*
// @include       https://*.he.reddit.com/*
// @include       http://hi.reddit.com/*
// @include       https://hi.reddit.com/*
// @include       http://*.hi.reddit.com/*
// @include       https://*.hi.reddit.com/*
// @include       http://hr.reddit.com/*
// @include       https://hr.reddit.com/*
// @include       http://*.hr.reddit.com/*
// @include       https://*.hr.reddit.com/*
// @include       http://hu.reddit.com/*
// @include       https://hu.reddit.com/*
// @include       http://*.hu.reddit.com/*
// @include       https://*.hu.reddit.com/*
// @include       http://hy.reddit.com/*
// @include       https://hy.reddit.com/*
// @include       http://*.hy.reddit.com/*
// @include       https://*.hy.reddit.com/*
// @include       http://id.reddit.com/*
// @include       https://id.reddit.com/*
// @include       http://*.id.reddit.com/*
// @include       https://*.id.reddit.com/*
// @include       http://is.reddit.com/*
// @include       https://is.reddit.com/*
// @include       http://*.is.reddit.com/*
// @include       https://*.is.reddit.com/*
// @include       http://it.reddit.com/*
// @include       https://it.reddit.com/*
// @include       http://*.it.reddit.com/*
// @include       https://*.it.reddit.com/*
// @include       http://ja.reddit.com/*
// @include       https://ja.reddit.com/*
// @include       http://*.ja.reddit.com/*
// @include       https://*.ja.reddit.com/*
// @include       http://kn_IN.reddit.com/*
// @include       https://kn_IN.reddit.com/*
// @include       http://*.kn_IN.reddit.com/*
// @include       https://*.kn_IN.reddit.com/*
// @include       http://ko.reddit.com/*
// @include       https://ko.reddit.com/*
// @include       http://*.ko.reddit.com/*
// @include       https://*.ko.reddit.com/*
// @include       http://la.reddit.com/*
// @include       https://la.reddit.com/*
// @include       http://*.la.reddit.com/*
// @include       https://*.la.reddit.com/*
// @include       http://leet.reddit.com/*
// @include       https://leet.reddit.com/*
// @include       http://*.leet.reddit.com/*
// @include       https://*.leet.reddit.com/*
// @include       http://lol.reddit.com/*
// @include       https://lol.reddit.com/*
// @include       http://*.lol.reddit.com/*
// @include       https://*.lol.reddit.com/*
// @include       http://lt.reddit.com/*
// @include       https://lt.reddit.com/*
// @include       http://*.lt.reddit.com/*
// @include       https://*.lt.reddit.com/*
// @include       http://lv.reddit.com/*
// @include       https://lv.reddit.com/*
// @include       http://*.lv.reddit.com/*
// @include       https://*.lv.reddit.com/*
// @include       http://mod.reddit.com/*
// @include       https://mod.reddit.com/*
// @include       http://*.mod.reddit.com/*
// @include       https://*.mod.reddit.com/*
// @include       http://ms.reddit.com/*
// @include       https://ms.reddit.com/*
// @include       http://*.ms.reddit.com/*
// @include       https://*.ms.reddit.com/*
// @include       http://mt-MT.reddit.com/*
// @include       https://mt-MT.reddit.com/*
// @include       http://*.mt-MT.reddit.com/*
// @include       https://*.mt-MT.reddit.com/*
// @include       http://nl.reddit.com/*
// @include       https://nl.reddit.com/*
// @include       http://*.nl.reddit.com/*
// @include       https://*.nl.reddit.com/*
// @include       http://nn.reddit.com/*
// @include       https://nn.reddit.com/*
// @include       http://*.nn.reddit.com/*
// @include       https://*.nn.reddit.com/*
// @include       http://no.reddit.com/*
// @include       https://no.reddit.com/*
// @include       http://*.no.reddit.com/*
// @include       https://*.no.reddit.com/*
// @include       http://oauth.reddit.com/*
// @include       https://oauth.reddit.com/*
// @include       http://*.oauth.reddit.com/*
// @include       https://*.oauth.reddit.com/*
// @include       http://pir.reddit.com/*
// @include       https://pir.reddit.com/*
// @include       http://*.pir.reddit.com/*
// @include       https://*.pir.reddit.com/*
// @include       http://pl.reddit.com/*
// @include       https://pl.reddit.com/*
// @include       http://*.pl.reddit.com/*
// @include       https://*.pl.reddit.com/*
// @include       http://pt.reddit.com/*
// @include       https://pt.reddit.com/*
// @include       http://*.pt.reddit.com/*
// @include       https://*.pt.reddit.com/*
// @include       http://pt-pt.reddit.com/*
// @include       https://pt-pt.reddit.com/*
// @include       http://*.pt-pt.reddit.com/*
// @include       https://*.pt-pt.reddit.com/*
// @include       http://pt_BR.reddit.com/*
// @include       https://pt_BR.reddit.com/*
// @include       http://*.pt_BR.reddit.com/*
// @include       https://*.pt_BR.reddit.com/*
// @include       http://ro.reddit.com/*
// @include       https://ro.reddit.com/*
// @include       http://*.ro.reddit.com/*
// @include       https://*.ro.reddit.com/*
// @include       http://ru.reddit.com/*
// @include       https://ru.reddit.com/*
// @include       http://*.ru.reddit.com/*
// @include       https://*.ru.reddit.com/*
// @include       http://sk.reddit.com/*
// @include       https://sk.reddit.com/*
// @include       http://*.sk.reddit.com/*
// @include       https://*.sk.reddit.com/*
// @include       http://sr.reddit.com/*
// @include       https://sr.reddit.com/*
// @include       http://*.sr.reddit.com/*
// @include       https://*.sr.reddit.com/*
// @include       http://sr-la.reddit.com/*
// @include       https://sr-la.reddit.com/*
// @include       http://*.sr-la.reddit.com/*
// @include       https://*.sr-la.reddit.com/*
// @include       http://sv.reddit.com/*
// @include       https://sv.reddit.com/*
// @include       http://*.sv.reddit.com/*
// @include       https://*.sv.reddit.com/*
// @include       http://ta.reddit.com/*
// @include       https://ta.reddit.com/*
// @include       http://*.ta.reddit.com/*
// @include       https://*.ta.reddit.com/*
// @include       http://th.reddit.com/*
// @include       https://th.reddit.com/*
// @include       http://*.th.reddit.com/*
// @include       https://*.th.reddit.com/*
// @include       http://tr.reddit.com/*
// @include       https://tr.reddit.com/*
// @include       http://*.tr.reddit.com/*
// @include       https://*.tr.reddit.com/*
// @include       http://uk.reddit.com/*
// @include       https://uk.reddit.com/*
// @include       http://*.uk.reddit.com/*
// @include       https://*.uk.reddit.com/*
// @include       http://us-en.reddit.com/*
// @include       https://us-en.reddit.com/*
// @include       http://*.us-en.reddit.com/*
// @include       https://*.us-en.reddit.com/*
// @include       http://us.reddit.com/*
// @include       https://us.reddit.com/*
// @include       http://*.us.reddit.com/*
// @include       https://*.us.reddit.com/*
// @include       http://vi.reddit.com/*
// @include       https://vi.reddit.com/*
// @include       http://*.vi.reddit.com/*
// @include       https://*.vi.reddit.com/*
// @include       http://zh.reddit.com/*
// @include       https://zh.reddit.com/*
// @include       http://*.zh.reddit.com/*
// @include       https://*.zh.reddit.com/*
// @include       http://zh.cn.reddit.com/*
// @include       https://zh.cn.reddit.com/*
// @include       http://*.zh.cn.reddit.com/*
// @include       https://*.zh.cn.reddit.com/*
// @include       http://translate.google.com/*
// @include       https://translate.google.com/*
// @include       http://*.translate.google.com/*
// @include       https://*.translate.google.com/*
// @include       http://translate.googleusercontent.com/*
// @include       https://translate.googleusercontent.com/*
// @include       http://*.translate.googleusercontent.com/*
// @include       https://*.translate.googleusercontent.com/*
// @include       http://www.reddit.com/*
// @include       https://www.reddit.com/*
// @include       http://*.www.reddit.com/*
// @include       https://*.www.reddit.com/*
// @include       http://old.reddit.com/*
// @include       https://old.reddit.com/*
// @include       http://*.old.reddit.com/*
// @include       https://*.old.reddit.com/*
// @include       http://np.reddit.com/*
// @include       https://np.reddit.com/*
// @include       http://*.np.reddit.com/*
// @include       https://*.np.reddit.com/*
// @include       http://snew.notabug.io/*
// @include       https://snew.notabug.io/*
// @include       http://*.snew.notabug.io/*
// @include       https://*.snew.notabug.io/*
// @include       http://notabug.io/*
// @include       https://notabug.io/*
// @include       http://*.notabug.io/*
// @include       https://*.notabug.io/*
// @include       http://af.reddit.com/*
// @include       https://af.reddit.com/*
// @include       http://*.af.reddit.com/*
// @include       https://*.af.reddit.com/*
// @include       http://ar.reddit.com/*
// @include       https://ar.reddit.com/*
// @include       http://*.ar.reddit.com/*
// @include       https://*.ar.reddit.com/*
// @include       http://be.reddit.com/*
// @include       https://be.reddit.com/*
// @include       http://*.be.reddit.com/*
// @include       https://*.be.reddit.com/*
// @include       http://bg.reddit.com/*
// @include       https://bg.reddit.com/*
// @include       http://*.bg.reddit.com/*
// @include       https://*.bg.reddit.com/*
// @include       http://bn-IN.reddit.com/*
// @include       https://bn-IN.reddit.com/*
// @include       http://*.bn-IN.reddit.com/*
// @include       https://*.bn-IN.reddit.com/*
// @include       http://bn.bd.reddit.com/*
// @include       https://bn.bd.reddit.com/*
// @include       http://*.bn.bd.reddit.com/*
// @include       https://*.bn.bd.reddit.com/*
// @include       http://bs.reddit.comx/*
// @include       https://bs.reddit.comx/*
// @include       http://*.bs.reddit.comx/*
// @include       https://*.bs.reddit.comx/*
// @include       http://ca.reddit.com/*
// @include       https://ca.reddit.com/*
// @include       http://*.ca.reddit.com/*
// @include       https://*.ca.reddit.com/*
// @include       http://cs.reddit.com/*
// @include       https://cs.reddit.com/*
// @include       http://*.cs.reddit.com/*
// @include       https://*.cs.reddit.com/*
// @include       http://cy.reddit.com/*
// @include       https://cy.reddit.com/*
// @include       http://*.cy.reddit.com/*
// @include       https://*.cy.reddit.com/*
// @include       http://da.reddit.com/*
// @include       https://da.reddit.com/*
// @include       http://*.da.reddit.com/*
// @include       https://*.da.reddit.com/*
// @include       http://dd.reddit.com/*
// @include       https://dd.reddit.com/*
// @include       http://*.dd.reddit.com/*
// @include       https://*.dd.reddit.com/*
// @include       http://de.reddit.com/*
// @include       https://de.reddit.com/*
// @include       http://*.de.reddit.com/*
// @include       https://*.de.reddit.com/*
// @include       http://ds.reddit.com/*
// @include       https://ds.reddit.com/*
// @include       http://*.ds.reddit.com/*
// @include       https://*.ds.reddit.com/*
// @include       http://el.reddit.com/*
// @include       https://el.reddit.com/*
// @include       http://*.el.reddit.com/*
// @include       https://*.el.reddit.com/*
// @include       http://en.reddit.com/*
// @include       https://en.reddit.com/*
// @include       http://*.en.reddit.com/*
// @include       https://*.en.reddit.com/*
// @include       http://en.au.reddit.com/*
// @include       https://en.au.reddit.com/*
// @include       http://*.en.au.reddit.com/*
// @include       https://*.en.au.reddit.com/*
// @include       http://en.ca.reddit.com/*
// @include       https://en.ca.reddit.com/*
// @include       http://*.en.ca.reddit.com/*
// @include       https://*.en.ca.reddit.com/*
// @include       http://en.gb.reddit.com/*
// @include       https://en.gb.reddit.com/*
// @include       http://*.en.gb.reddit.com/*
// @include       https://*.en.gb.reddit.com/*
// @include       http://en.us.reddit.com/*
// @include       https://en.us.reddit.com/*
// @include       http://*.en.us.reddit.com/*
// @include       https://*.en.us.reddit.com/*
// @include       http://eo.reddit.com/*
// @include       https://eo.reddit.com/*
// @include       http://*.eo.reddit.com/*
// @include       https://*.eo.reddit.com/*
// @include       http://es.reddit.com/*
// @include       https://es.reddit.com/*
// @include       http://*.es.reddit.com/*
// @include       https://*.es.reddit.com/*
// @include       http://es-ar.reddit.com/*
// @include       https://es-ar.reddit.com/*
// @include       http://*.es-ar.reddit.com/*
// @include       https://*.es-ar.reddit.com/*
// @include       http://es-cl.reddit.com/*
// @include       https://es-cl.reddit.com/*
// @include       http://*.es-cl.reddit.com/*
// @include       https://*.es-cl.reddit.com/*
// @include       http://es-mx.reddit.com/*
// @include       https://es-mx.reddit.com/*
// @include       http://*.es-mx.reddit.com/*
// @include       https://*.es-mx.reddit.com/*
// @include       http://et.reddit.com/*
// @include       https://et.reddit.com/*
// @include       http://*.et.reddit.com/*
// @include       https://*.et.reddit.com/*
// @include       http://eu.reddit.com/*
// @include       https://eu.reddit.com/*
// @include       http://*.eu.reddit.com/*
// @include       https://*.eu.reddit.com/*
// @include       http://fa.reddit.com/*
// @include       https://fa.reddit.com/*
// @include       http://*.fa.reddit.com/*
// @include       https://*.fa.reddit.com/*
// @include       http://fi.reddit.com/*
// @include       https://fi.reddit.com/*
// @include       http://*.fi.reddit.com/*
// @include       https://*.fi.reddit.com/*
// @include       http://fil.reddit.com/*
// @include       https://fil.reddit.com/*
// @include       http://*.fil.reddit.com/*
// @include       https://*.fil.reddit.com/*
// @include       http://fr.reddit.com.reddit.com/*
// @include       https://fr.reddit.com.reddit.com/*
// @include       http://*.fr.reddit.com.reddit.com/*
// @include       https://*.fr.reddit.com.reddit.com/*
// @include       http://fr.ca.reddit.com/*
// @include       https://fr.ca.reddit.com/*
// @include       http://*.fr.ca.reddit.com/*
// @include       https://*.fr.ca.reddit.com/*
// @include       http://fy-NL.reddit.com/*
// @include       https://fy-NL.reddit.com/*
// @include       http://*.fy-NL.reddit.com/*
// @include       https://*.fy-NL.reddit.com/*
// @include       http://ga-ie.reddit.com/*
// @include       https://ga-ie.reddit.com/*
// @include       http://*.ga-ie.reddit.com/*
// @include       https://*.ga-ie.reddit.com/*
// @include       http://gd.reddit.com/*
// @include       https://gd.reddit.com/*
// @include       http://*.gd.reddit.com/*
// @include       https://*.gd.reddit.com/*
// @include       http://gl.reddit.com/*
// @include       https://gl.reddit.com/*
// @include       http://*.gl.reddit.com/*
// @include       https://*.gl.reddit.com/*
// @include       http://he.reddit.com/*
// @include       https://he.reddit.com/*
// @include       http://*.he.reddit.com/*
// @include       https://*.he.reddit.com/*
// @include       http://hi.reddit.com/*
// @include       https://hi.reddit.com/*
// @include       http://*.hi.reddit.com/*
// @include       https://*.hi.reddit.com/*
// @include       http://hr.reddit.com/*
// @include       https://hr.reddit.com/*
// @include       http://*.hr.reddit.com/*
// @include       https://*.hr.reddit.com/*
// @include       http://hu.reddit.com/*
// @include       https://hu.reddit.com/*
// @include       http://*.hu.reddit.com/*
// @include       https://*.hu.reddit.com/*
// @include       http://hy.reddit.com/*
// @include       https://hy.reddit.com/*
// @include       http://*.hy.reddit.com/*
// @include       https://*.hy.reddit.com/*
// @include       http://id.reddit.com/*
// @include       https://id.reddit.com/*
// @include       http://*.id.reddit.com/*
// @include       https://*.id.reddit.com/*
// @include       http://is.reddit.com/*
// @include       https://is.reddit.com/*
// @include       http://*.is.reddit.com/*
// @include       https://*.is.reddit.com/*
// @include       http://it.reddit.com/*
// @include       https://it.reddit.com/*
// @include       http://*.it.reddit.com/*
// @include       https://*.it.reddit.com/*
// @include       http://ja.reddit.com/*
// @include       https://ja.reddit.com/*
// @include       http://*.ja.reddit.com/*
// @include       https://*.ja.reddit.com/*
// @include       http://kn_IN.reddit.com/*
// @include       https://kn_IN.reddit.com/*
// @include       http://*.kn_IN.reddit.com/*
// @include       https://*.kn_IN.reddit.com/*
// @include       http://ko.reddit.com/*
// @include       https://ko.reddit.com/*
// @include       http://*.ko.reddit.com/*
// @include       https://*.ko.reddit.com/*
// @include       http://la.reddit.com/*
// @include       https://la.reddit.com/*
// @include       http://*.la.reddit.com/*
// @include       https://*.la.reddit.com/*
// @include       http://leet.reddit.com/*
// @include       https://leet.reddit.com/*
// @include       http://*.leet.reddit.com/*
// @include       https://*.leet.reddit.com/*
// @include       http://lol.reddit.com/*
// @include       https://lol.reddit.com/*
// @include       http://*.lol.reddit.com/*
// @include       https://*.lol.reddit.com/*
// @include       http://lt.reddit.com/*
// @include       https://lt.reddit.com/*
// @include       http://*.lt.reddit.com/*
// @include       https://*.lt.reddit.com/*
// @include       http://lv.reddit.com/*
// @include       https://lv.reddit.com/*
// @include       http://*.lv.reddit.com/*
// @include       https://*.lv.reddit.com/*
// @include       http://mod.reddit.com/*
// @include       https://mod.reddit.com/*
// @include       http://*.mod.reddit.com/*
// @include       https://*.mod.reddit.com/*
// @include       http://ms.reddit.com/*
// @include       https://ms.reddit.com/*
// @include       http://*.ms.reddit.com/*
// @include       https://*.ms.reddit.com/*
// @include       http://mt-MT.reddit.com/*
// @include       https://mt-MT.reddit.com/*
// @include       http://*.mt-MT.reddit.com/*
// @include       https://*.mt-MT.reddit.com/*
// @include       http://nl.reddit.com/*
// @include       https://nl.reddit.com/*
// @include       http://*.nl.reddit.com/*
// @include       https://*.nl.reddit.com/*
// @include       http://nn.reddit.com/*
// @include       https://nn.reddit.com/*
// @include       http://*.nn.reddit.com/*
// @include       https://*.nn.reddit.com/*
// @include       http://no.reddit.com/*
// @include       https://no.reddit.com/*
// @include       http://*.no.reddit.com/*
// @include       https://*.no.reddit.com/*
// @include       http://oauth.reddit.com/*
// @include       https://oauth.reddit.com/*
// @include       http://*.oauth.reddit.com/*
// @include       https://*.oauth.reddit.com/*
// @include       http://pir.reddit.com/*
// @include       https://pir.reddit.com/*
// @include       http://*.pir.reddit.com/*
// @include       https://*.pir.reddit.com/*
// @include       http://pl.reddit.com/*
// @include       https://pl.reddit.com/*
// @include       http://*.pl.reddit.com/*
// @include       https://*.pl.reddit.com/*
// @include       http://pt.reddit.com/*
// @include       https://pt.reddit.com/*
// @include       http://*.pt.reddit.com/*
// @include       https://*.pt.reddit.com/*
// @include       http://pt-pt.reddit.com/*
// @include       https://pt-pt.reddit.com/*
// @include       http://*.pt-pt.reddit.com/*
// @include       https://*.pt-pt.reddit.com/*
// @include       http://pt_BR.reddit.com/*
// @include       https://pt_BR.reddit.com/*
// @include       http://*.pt_BR.reddit.com/*
// @include       https://*.pt_BR.reddit.com/*
// @include       http://ro.reddit.com/*
// @include       https://ro.reddit.com/*
// @include       http://*.ro.reddit.com/*
// @include       https://*.ro.reddit.com/*
// @include       http://ru.reddit.com/*
// @include       https://ru.reddit.com/*
// @include       http://*.ru.reddit.com/*
// @include       https://*.ru.reddit.com/*
// @include       http://sk.reddit.com/*
// @include       https://sk.reddit.com/*
// @include       http://*.sk.reddit.com/*
// @include       https://*.sk.reddit.com/*
// @include       http://sr.reddit.com/*
// @include       https://sr.reddit.com/*
// @include       http://*.sr.reddit.com/*
// @include       https://*.sr.reddit.com/*
// @include       http://sr-la.reddit.com/*
// @include       https://sr-la.reddit.com/*
// @include       http://*.sr-la.reddit.com/*
// @include       https://*.sr-la.reddit.com/*
// @include       http://sv.reddit.com/*
// @include       https://sv.reddit.com/*
// @include       http://*.sv.reddit.com/*
// @include       https://*.sv.reddit.com/*
// @include       http://ta.reddit.com/*
// @include       https://ta.reddit.com/*
// @include       http://*.ta.reddit.com/*
// @include       https://*.ta.reddit.com/*
// @include       http://th.reddit.com/*
// @include       https://th.reddit.com/*
// @include       http://*.th.reddit.com/*
// @include       https://*.th.reddit.com/*
// @include       http://tr.reddit.com/*
// @include       https://tr.reddit.com/*
// @include       http://*.tr.reddit.com/*
// @include       https://*.tr.reddit.com/*
// @include       http://uk.reddit.com/*
// @include       https://uk.reddit.com/*
// @include       http://*.uk.reddit.com/*
// @include       https://*.uk.reddit.com/*
// @include       http://us-en.reddit.com/*
// @include       https://us-en.reddit.com/*
// @include       http://*.us-en.reddit.com/*
// @include       https://*.us-en.reddit.com/*
// @include       http://us.reddit.com/*
// @include       https://us.reddit.com/*
// @include       http://*.us.reddit.com/*
// @include       https://*.us.reddit.com/*
// @include       http://vi.reddit.com/*
// @include       https://vi.reddit.com/*
// @include       http://*.vi.reddit.com/*
// @include       https://*.vi.reddit.com/*
// @include       http://zh.reddit.com/*
// @include       https://zh.reddit.com/*
// @include       http://*.zh.reddit.com/*
// @include       https://*.zh.reddit.com/*
// @include       http://zh.cn.reddit.com/*
// @include       https://zh.cn.reddit.com/*
// @include       http://*.zh.cn.reddit.com/*
// @include       https://*.zh.cn.reddit.com/*
// @include       http://translate.google.com/*
// @include       https://translate.google.com/*
// @include       http://*.translate.google.com/*
// @include       https://*.translate.google.com/*
// @include       http://translate.googleusercontent.com/*
// @include       https://translate.googleusercontent.com/*
// @include       http://*.translate.googleusercontent.com/*
// @include       https://*.translate.googleusercontent.com/*
// @run-at        document-start
// @version       0.20191110052849
// ==/UserScript==
(function() {var css = "";
if (false || (document.domain == "www.reddit.com" || document.domain.substring(document.domain.indexOf(".www.reddit.com") + 1) == "www.reddit.com") || (document.domain == "old.reddit.com" || document.domain.substring(document.domain.indexOf(".old.reddit.com") + 1) == "old.reddit.com") || (document.domain == "np.reddit.com" || document.domain.substring(document.domain.indexOf(".np.reddit.com") + 1) == "np.reddit.com") || (document.domain == "snew.notabug.io" || document.domain.substring(document.domain.indexOf(".snew.notabug.io") + 1) == "snew.notabug.io") || (document.domain == "notabug.io" || document.domain.substring(document.domain.indexOf(".notabug.io") + 1) == "notabug.io") || (document.domain == "af.reddit.com" || document.domain.substring(document.domain.indexOf(".af.reddit.com") + 1) == "af.reddit.com") || (document.domain == "ar.reddit.com" || document.domain.substring(document.domain.indexOf(".ar.reddit.com") + 1) == "ar.reddit.com") || (document.domain == "be.reddit.com" || document.domain.substring(document.domain.indexOf(".be.reddit.com") + 1) == "be.reddit.com") || (document.domain == "bg.reddit.com" || document.domain.substring(document.domain.indexOf(".bg.reddit.com") + 1) == "bg.reddit.com") || (document.domain == "bn-IN.reddit.com" || document.domain.substring(document.domain.indexOf(".bn-IN.reddit.com") + 1) == "bn-IN.reddit.com") || (document.domain == "bn.bd.reddit.com" || document.domain.substring(document.domain.indexOf(".bn.bd.reddit.com") + 1) == "bn.bd.reddit.com") || (document.domain == "bs.reddit.comx" || document.domain.substring(document.domain.indexOf(".bs.reddit.comx") + 1) == "bs.reddit.comx") || (document.domain == "ca.reddit.com" || document.domain.substring(document.domain.indexOf(".ca.reddit.com") + 1) == "ca.reddit.com") || (document.domain == "cs.reddit.com" || document.domain.substring(document.domain.indexOf(".cs.reddit.com") + 1) == "cs.reddit.com") || (document.domain == "cy.reddit.com" || document.domain.substring(document.domain.indexOf(".cy.reddit.com") + 1) == "cy.reddit.com") || (document.domain == "da.reddit.com" || document.domain.substring(document.domain.indexOf(".da.reddit.com") + 1) == "da.reddit.com") || (document.domain == "dd.reddit.com" || document.domain.substring(document.domain.indexOf(".dd.reddit.com") + 1) == "dd.reddit.com") || (document.domain == "de.reddit.com" || document.domain.substring(document.domain.indexOf(".de.reddit.com") + 1) == "de.reddit.com") || (document.domain == "ds.reddit.com" || document.domain.substring(document.domain.indexOf(".ds.reddit.com") + 1) == "ds.reddit.com") || (document.domain == "el.reddit.com" || document.domain.substring(document.domain.indexOf(".el.reddit.com") + 1) == "el.reddit.com") || (document.domain == "en.reddit.com" || document.domain.substring(document.domain.indexOf(".en.reddit.com") + 1) == "en.reddit.com") || (document.domain == "en.au.reddit.com" || document.domain.substring(document.domain.indexOf(".en.au.reddit.com") + 1) == "en.au.reddit.com") || (document.domain == "en.ca.reddit.com" || document.domain.substring(document.domain.indexOf(".en.ca.reddit.com") + 1) == "en.ca.reddit.com") || (document.domain == "en.gb.reddit.com" || document.domain.substring(document.domain.indexOf(".en.gb.reddit.com") + 1) == "en.gb.reddit.com") || (document.domain == "en.us.reddit.com" || document.domain.substring(document.domain.indexOf(".en.us.reddit.com") + 1) == "en.us.reddit.com") || (document.domain == "eo.reddit.com" || document.domain.substring(document.domain.indexOf(".eo.reddit.com") + 1) == "eo.reddit.com") || (document.domain == "es.reddit.com" || document.domain.substring(document.domain.indexOf(".es.reddit.com") + 1) == "es.reddit.com") || (document.domain == "es-ar.reddit.com" || document.domain.substring(document.domain.indexOf(".es-ar.reddit.com") + 1) == "es-ar.reddit.com") || (document.domain == "es-cl.reddit.com" || document.domain.substring(document.domain.indexOf(".es-cl.reddit.com") + 1) == "es-cl.reddit.com") || (document.domain == "es-mx.reddit.com" || document.domain.substring(document.domain.indexOf(".es-mx.reddit.com") + 1) == "es-mx.reddit.com") || (document.domain == "et.reddit.com" || document.domain.substring(document.domain.indexOf(".et.reddit.com") + 1) == "et.reddit.com") || (document.domain == "eu.reddit.com" || document.domain.substring(document.domain.indexOf(".eu.reddit.com") + 1) == "eu.reddit.com") || (document.domain == "fa.reddit.com" || document.domain.substring(document.domain.indexOf(".fa.reddit.com") + 1) == "fa.reddit.com") || (document.domain == "fi.reddit.com" || document.domain.substring(document.domain.indexOf(".fi.reddit.com") + 1) == "fi.reddit.com") || (document.domain == "fil.reddit.com" || document.domain.substring(document.domain.indexOf(".fil.reddit.com") + 1) == "fil.reddit.com") || (document.domain == "fr.reddit.com.reddit.com" || document.domain.substring(document.domain.indexOf(".fr.reddit.com.reddit.com") + 1) == "fr.reddit.com.reddit.com") || (document.domain == "fr.ca.reddit.com" || document.domain.substring(document.domain.indexOf(".fr.ca.reddit.com") + 1) == "fr.ca.reddit.com") || (document.domain == "fy-NL.reddit.com" || document.domain.substring(document.domain.indexOf(".fy-NL.reddit.com") + 1) == "fy-NL.reddit.com") || (document.domain == "ga-ie.reddit.com" || document.domain.substring(document.domain.indexOf(".ga-ie.reddit.com") + 1) == "ga-ie.reddit.com") || (document.domain == "gd.reddit.com" || document.domain.substring(document.domain.indexOf(".gd.reddit.com") + 1) == "gd.reddit.com") || (document.domain == "gl.reddit.com" || document.domain.substring(document.domain.indexOf(".gl.reddit.com") + 1) == "gl.reddit.com") || (document.domain == "he.reddit.com" || document.domain.substring(document.domain.indexOf(".he.reddit.com") + 1) == "he.reddit.com") || (document.domain == "hi.reddit.com" || document.domain.substring(document.domain.indexOf(".hi.reddit.com") + 1) == "hi.reddit.com") || (document.domain == "hr.reddit.com" || document.domain.substring(document.domain.indexOf(".hr.reddit.com") + 1) == "hr.reddit.com") || (document.domain == "hu.reddit.com" || document.domain.substring(document.domain.indexOf(".hu.reddit.com") + 1) == "hu.reddit.com") || (document.domain == "hy.reddit.com" || document.domain.substring(document.domain.indexOf(".hy.reddit.com") + 1) == "hy.reddit.com") || (document.domain == "id.reddit.com" || document.domain.substring(document.domain.indexOf(".id.reddit.com") + 1) == "id.reddit.com") || (document.domain == "is.reddit.com" || document.domain.substring(document.domain.indexOf(".is.reddit.com") + 1) == "is.reddit.com") || (document.domain == "it.reddit.com" || document.domain.substring(document.domain.indexOf(".it.reddit.com") + 1) == "it.reddit.com") || (document.domain == "ja.reddit.com" || document.domain.substring(document.domain.indexOf(".ja.reddit.com") + 1) == "ja.reddit.com") || (document.domain == "kn_IN.reddit.com" || document.domain.substring(document.domain.indexOf(".kn_IN.reddit.com") + 1) == "kn_IN.reddit.com") || (document.domain == "ko.reddit.com" || document.domain.substring(document.domain.indexOf(".ko.reddit.com") + 1) == "ko.reddit.com") || (document.domain == "la.reddit.com" || document.domain.substring(document.domain.indexOf(".la.reddit.com") + 1) == "la.reddit.com") || (document.domain == "leet.reddit.com" || document.domain.substring(document.domain.indexOf(".leet.reddit.com") + 1) == "leet.reddit.com") || (document.domain == "lol.reddit.com" || document.domain.substring(document.domain.indexOf(".lol.reddit.com") + 1) == "lol.reddit.com") || (document.domain == "lt.reddit.com" || document.domain.substring(document.domain.indexOf(".lt.reddit.com") + 1) == "lt.reddit.com") || (document.domain == "lv.reddit.com" || document.domain.substring(document.domain.indexOf(".lv.reddit.com") + 1) == "lv.reddit.com") || (document.domain == "mod.reddit.com" || document.domain.substring(document.domain.indexOf(".mod.reddit.com") + 1) == "mod.reddit.com") || (document.domain == "ms.reddit.com" || document.domain.substring(document.domain.indexOf(".ms.reddit.com") + 1) == "ms.reddit.com") || (document.domain == "mt-MT.reddit.com" || document.domain.substring(document.domain.indexOf(".mt-MT.reddit.com") + 1) == "mt-MT.reddit.com") || (document.domain == "nl.reddit.com" || document.domain.substring(document.domain.indexOf(".nl.reddit.com") + 1) == "nl.reddit.com") || (document.domain == "nn.reddit.com" || document.domain.substring(document.domain.indexOf(".nn.reddit.com") + 1) == "nn.reddit.com") || (document.domain == "no.reddit.com" || document.domain.substring(document.domain.indexOf(".no.reddit.com") + 1) == "no.reddit.com") || (document.domain == "oauth.reddit.com" || document.domain.substring(document.domain.indexOf(".oauth.reddit.com") + 1) == "oauth.reddit.com") || (document.domain == "pir.reddit.com" || document.domain.substring(document.domain.indexOf(".pir.reddit.com") + 1) == "pir.reddit.com") || (document.domain == "pl.reddit.com" || document.domain.substring(document.domain.indexOf(".pl.reddit.com") + 1) == "pl.reddit.com") || (document.domain == "pt.reddit.com" || document.domain.substring(document.domain.indexOf(".pt.reddit.com") + 1) == "pt.reddit.com") || (document.domain == "pt-pt.reddit.com" || document.domain.substring(document.domain.indexOf(".pt-pt.reddit.com") + 1) == "pt-pt.reddit.com") || (document.domain == "pt_BR.reddit.com" || document.domain.substring(document.domain.indexOf(".pt_BR.reddit.com") + 1) == "pt_BR.reddit.com") || (document.domain == "ro.reddit.com" || document.domain.substring(document.domain.indexOf(".ro.reddit.com") + 1) == "ro.reddit.com") || (document.domain == "ru.reddit.com" || document.domain.substring(document.domain.indexOf(".ru.reddit.com") + 1) == "ru.reddit.com") || (document.domain == "sk.reddit.com" || document.domain.substring(document.domain.indexOf(".sk.reddit.com") + 1) == "sk.reddit.com") || (document.domain == "sr.reddit.com" || document.domain.substring(document.domain.indexOf(".sr.reddit.com") + 1) == "sr.reddit.com") || (document.domain == "sr-la.reddit.com" || document.domain.substring(document.domain.indexOf(".sr-la.reddit.com") + 1) == "sr-la.reddit.com") || (document.domain == "sv.reddit.com" || document.domain.substring(document.domain.indexOf(".sv.reddit.com") + 1) == "sv.reddit.com") || (document.domain == "ta.reddit.com" || document.domain.substring(document.domain.indexOf(".ta.reddit.com") + 1) == "ta.reddit.com") || (document.domain == "th.reddit.com" || document.domain.substring(document.domain.indexOf(".th.reddit.com") + 1) == "th.reddit.com") || (document.domain == "tr.reddit.com" || document.domain.substring(document.domain.indexOf(".tr.reddit.com") + 1) == "tr.reddit.com") || (document.domain == "uk.reddit.com" || document.domain.substring(document.domain.indexOf(".uk.reddit.com") + 1) == "uk.reddit.com") || (document.domain == "us-en.reddit.com" || document.domain.substring(document.domain.indexOf(".us-en.reddit.com") + 1) == "us-en.reddit.com") || (document.domain == "us.reddit.com" || document.domain.substring(document.domain.indexOf(".us.reddit.com") + 1) == "us.reddit.com") || (document.domain == "vi.reddit.com" || document.domain.substring(document.domain.indexOf(".vi.reddit.com") + 1) == "vi.reddit.com") || (document.domain == "zh.reddit.com" || document.domain.substring(document.domain.indexOf(".zh.reddit.com") + 1) == "zh.reddit.com") || (document.domain == "zh.cn.reddit.com" || document.domain.substring(document.domain.indexOf(".zh.cn.reddit.com") + 1) == "zh.cn.reddit.com") || (document.domain == "translate.google.com" || document.domain.substring(document.domain.indexOf(".translate.google.com") + 1) == "translate.google.com") || (document.domain == "translate.googleusercontent.com" || document.domain.substring(document.domain.indexOf(".translate.googleusercontent.com") + 1) == "translate.googleusercontent.com"))
	css += [
		"/*",
		"/*",
		"/* Reddit Dark Theme - KMV",
		"/* https://userstyles.org/styles/174881/",
		"/*",
		"/* Designed for STYLUS:",
		"/* https://add0n.com/stylus.html",
		"/*",
		"/* Complimentary script for further dimming;",
		"/* Reddit Media Dim - KMV",
		"/* https://userstyles.org/styles/176259/",
		"/*",
		"/* Other Themes by KMV:",
		"/* https://userstyles.org/users/370722",
		"/*",
		"/* Contact: http://www.reddit.com/r/KMV/",
		"/*",
		"*/",
		"::selection,",
		"*::selection,",
		"::-moz-selection,",
		"body *::-moz-selection",
		"{",
		"    color: #000 !important;",
		"    background-color: #b0b0b0 !important;",
		"}",
		"select",
		"{",
		"    color: #b0b0b0 !important;",
		"}",
		"",
		"textarea,",
		"pre,",
		"em,",
		"li,",
		"ol,",
		"th,",
		"ul,",
		"p",
		"{",
		"    background-color: transparent !important;",
		"    border: none !important;",
		"    color: #ccc !important;",
		"}",
		"/* Daylight Listings Title */",
		".thing .title,",
		".pagename a:hover,",
		"li p:hover,",
		"li strong:hover,",
		"._1nBP1OfpQDgTmzRFqaVult,",
		"._2aqH0n-kSzFY7HZZ5GL-Jb,",
		".vzhy90YD0qH7ZDJi7xMGw,",
		"._1zZ3VDhRC38fXLLvVCHOwK",
		"{",
		"    color: #fff !important;",
		"    transition: .4s !important;",
		"}",
		"",
		".thing .title:hover",
		"{",
		"    color: #fff !important;",
		"    transition: .4s !important;",
		"}",
		"",
		"/*Regular Titles*/",
		"div,",
		"h1,",
		"h2,",
		"h3,",
		"h4,",
		"h5,",
		"h6,",
		"label,",
		"li p,",
		"li strong,",
		".res-nightmode td,",
		"#header .user > a::after,",
		".commentingAsUser a,",
		".commentingAs,",
		".dropdown.srdrop .selected,",
		".hover-bubble.multi-selector strong,",
		".linkinfo .date,",
		".comments-page .linkinfo,",
		".res-nightmode .thing .title,",
		".res-nightmode form,",
		".RESDialogSmall,",
		".side *,",
		".titlebox::before,",
		".titlebox .tagline a.author,",
		".author.may-blank.id-t2_11ypoj,",
		"._1YWXCINvcuU7nk0ED-bta8,",
		"._1HGeWoy6faY2UWkCD7cYoW,",
		"._7JH6kQpO-bx66b9ajIZrz,",
		"._eYtD2XCVieq6emjKBH3m",
		"{",
		"    color: #ccc !important;",
		"}",
		"/*To be over-ridden by a cousin*/",
		"#header .user > .login-required",
		"{",
		"    color: #ccc;",
		"    transition: .4s;",
		"}",
		"",
		"/*Standard Text Color for selected Post Info*/",
		"li,",
		".res-nightmode #RESConsoleContainer,",
		".res-nightmode .moduleButton.enabled,",
		".res-nightmode .optionsTable th,",
		".res-nightmode .tagline .score,",
		".res-nightmode em,",
		".res-nightmode h1,",
		".res-nightmode h2,",
		".res-nightmode h3,",
		".res-nightmode h4,",
		".res-nightmode h5,",
		".res-nightmode h6,",
		".res-nightmode label,",
		".res-nightmode li,",
		".res-nightmode ol,",
		".res-nightmode th,",
		".res-nightmode p,",
		".res-nightmode pre,",
		".res-nightmode strong,",
		".res-nightmode ul,",
		"/*.res-nightmode ul a,*/",
		".res-nightmode .entry.res-selected .md-container > .md p,",
		".res-voteEnhancements-highlightScores span.score,",
		".RESHoverTitle,",
		".side .md p,",
		"body:not(.subscriber) .sidebox.submit-link::before,",
		".side ul li,",
		".md p,",
		".res-flairSearch.linkflairlabel",
		"{",
		"    background-color: transparent !important;",
		"    border: none !important;",
		"    color: #b4b4b4!important;",
		"}",
		"",
		"a em,",
		"div a,",
		"form a,",
		"h1 a,",
		"h2 a,",
		"h3 a,",
		"h4 a,",
		"h5 a,",
		"h6 a,",
		"li a,",
		"p a,",
		"span a,",
		"#header a.login-required.login-link,",
		"#side-mod-list.summarized a,",
		"#srList td a,",
		".md a,",
		".morelink a,",
		".pref-lang.choice,",
		".domain a,",
		".res-nightmode form a,",
		".side a,",
		".sr-bar a,",
		"._2H51id1RX9dGNrtrAIOMGK,",
		".EVdFfF5K7kZFMaYpSHbLe",
		"{",
		"    color: #409b9b !important;",
		"    transition: .4s !important;",
		"}",
		"",
		"a em:hover,",
		"div a:hover/*Observe*/",
		",",
		"/*li a:hover,*/",
		"h1 a:hover,",
		"h2 a:hover,",
		"h3 a:hover,",
		"h4 a:hover,",
		"h5 a:hover,",
		"h6 a:hover,",
		"p a:hover,",
		"#header a.login-required.login-link:hover,",
		"#side-mod-list.summarized a:hover,",
		"#srList td a:hover,",
		".entry .buttons a[onclick*=\"reply\"]:hover,",
		".md a:hover,",
		".morelink a:hover,",
		".pref-lang.choice:hover,",
		".res-nightmode form a:hover,",
		".side a:hover,",
		".sr-bar a:hover,",
		"._2H51id1RX9dGNrtrAIOMGK:hover",
		"{",
		"    color: #1abebe !important;",
		"    transition: .4s !important;",
		"}",
		"",
		"button,",
		".res-nightmode button",
		"{",
		"    background-color: #0000!important;",
		"    border: none!important;",
		"    color: #aaa !important;",
		"    transition: .4s;",
		"}",
		"",
		"button:hover,",
		".res-nightmode button:hover",
		"{",
		"    color: #fff !important;",
		"    transition: .4s;",
		"}",
		"",
		".pagename a,",
		".trophy-name,",
		".res-nightmode li p",
		"{",
		"    color: #999 !important;",
		"    transition: .4s !important;",
		"}",
		"",
		"/*Top Right RES Sub-Shortcuts*/",
		"#RESShortcutsSort,",
		"#RESShortcutsRight,",
		"#RESShortcutsLeft,",
		"#RESShortcutsAdd",
		"{",
		"    color: #999 !important;",
		"    transition: .4s;",
		"}",
		"#RESShortcutsSort:hover,",
		"#RESShortcutsRight:hover,",
		"#RESShortcutsLeft:hover,",
		"#RESShortcutsAdd:hover",
		"{",
		"    color: #fff !important;",
		"    transition: .4s;",
		"}",
		"#RESShortcutsAdd:hover",
		"{",
		"    color: #1abebe !important;",
		"    transition: .4s;",
		"}",
		"",
		".buttons a:hover a[data-click-id=\"body\"]",
		"{",
		"    color: #aaa !important;",
		"    transition: .4s !important;",
		"}",
		"",
		".res-nightmode form,",
		".res-nightmode table,",
		".res-nightmode td,",
		".res-nightmode tr,",
		".res-nightmode *:after,",
		".res-nightmode *:before,",
		".res-nightmode #header .user > a::after,",
		".res-nightmode .tagline .edited-timestamp,",
		".res-nightmode .search-result-meta .edited-timestamp",
		"{",
		"    background-color: transparent !important;",
		"    border: none !important;",
		"    color: #777 !important;",
		"    transition: .4s;",
		"}",
		"",
		"/*Sub-links Treatment*/",
		".entry .buttons li,",
		".entry .buttons li a,",
		".buttons a,",
		".buttons li",
		"{",
		"    color: #888 !important;",
		"    margin-right: 5px !important;",
		"    padding: 0 !important;",
		"    transition: .4s !important;",
		"}",
		".entry .buttons li:hover,",
		".entry .buttons li a:hover,",
		".buttons a:hover,",
		".buttons li:hover,",
		".res-nightmode li:hover",
		"{",
		"    color: #aaa !important;",
		"    transition: .4s !important;",
		"}",
		"",
		"",
		".title:visited,",
		"a[data-click-id=\"timestamp\"],",
		"a[data-click-id=\"body\"]:visited h2,",
		".tagline .edited-timestamp,",
		".search-result-meta .edited-timestamp",
		"{",
		"    border: none !important;",
		"    color: #777 !important;",
		"    transition: .4s !important;",
		"}",
		"",
		".buttons a:visited",
		"{",
		"    color: #666 !important;",
		"}",
		"",
		".res-nightmode .title:visited",
		"{",
		"    color: #555 !important;",
		"}",
		"",
		"/*Text Formatting*/",
		"/* Default links */",
		"a,",
		"a *",
		"{",
		"    border: none !important;",
		"}",
		"",
		".icon,",
		".icon:before,",
		".icon:hover",
		"{",
		"    color: inherit !important;",
		"    transition: .4s !important;",
		"}",
		"",
		".pagename,",
		".link .midcol,",
		".buttons a,",
		".subreddit",
		"{",
		"    font-weight: normal !important;",
		"}",
		"",
		".scrollerItem",
		"{",
		"    box-shadow: none !important;",
		"}",
		"",
		"/*Post Position and Spacing (old.reddit - Original - forced)*/",
		".link",
		"{",
		"    margin-bottom: 8px !important;",
		"    padding-left: 3px !important;",
		"}",
		"",
		"/*Boxed URL*/",
		"input#shortlink-text,",
		".shortlink input,",
		".linkinfo .shortlink input",
		"{",
		"    background-color: #000 !important;",
		"    border-radius: 2px!important;",
		"    color: #888 !important;",
		"    box-shadow: 0px 0px 5px 0px #333;",
		"    padding-left: 3px !important;",
		"}",
		"",
		"/*Code*/",
		"code,",
		".usertext-body code",
		"{",
		"    background-color: #000 !important;",
		"    border-radius: 2px!important;",
		"    color: #a0ffa0 !important;",
		"    box-shadow: 0px 0px 5px 0px #333;",
		"    padding-left: 3px !important;",
		"}",
		"",
		"",
		"/*Backgrounds*/",
		"div,",
		"form,",
		"label,",
		"section,",
		"span,",
		".res-nightmode span,",
		"strong,",
		"table,",
		"td,",
		"tr,",
		"*:before,",
		"*:after",
		"{",
		"    background-color: transparent !important;",
		"    border: none !important;",
		"}",
		"",
		".blueButton,",
		".btn,",
		".choice,",
		".fancy-toggle-button a,",
		".footer-parent,",
		".helplink.access-required,",
		".infobar.commentsignupbar,",
		".infobar-toaster-container,",
		".listing-page.hot-page,",
		".listingsignupbar__cta-container,",
		".listingsignupbar__close,",
		".login-form-side,",
		".morelink,",
		".morelink a,",
		".morelink:hover a,",
		".nextprev a,",
		".res-nightmode .nextprev a,",
		".res-nightmode .RESDropdownList li a,",
		".RESDropdownList li a,",
		".side,",
		".side .md blockquote,",
		".side .titlebox,",
		".sidecontentbox,",
		".sidecontentbox .content a.author,",
		".top-matter,",
		".users-online,",
		".subscribers,",
		".usertext button,",
		".voteWeight,",
		"._1YWXCINvcuU7nk0ED-bta8 *,",
		"._2nelDm85zKKmuD94NequP0,",
		"._3UhcBirLMXHSbN5vYYBGSF",
		"{",
		"    background: #0000 !important;",
		"    transition: .4s !important;",
		"}",
		"",
		"/*Transparent w/Border*/",
		".sidecontentbox .title h1,",
		".titlebox .usertext-body .md em a,",
		"._1BFbVxT49QnrAN3fqGZ1z8,",
		".side .titlebox .md h1,",
		".side .md > blockquote blockquote",
		"{",
		"    background: #0000 !important;",
		"    border-radius: 4px!important;",
		"    box-shadow: 0px 0px 2px 0px #333;",
		"}",
		"",
		"/*#000*/",
		"#RESShortcutsViewport",
		"{",
		"    background-color: #000 !important;",
		"}",
		"",
		"/*#000 w/border*/",
		"/*Preferences - Apps - Hover Popup*/",
		".app-scope",
		"{",
		"    background: #000 !important;",
		"    border-radius: 4px!important;",
		"    box-shadow: 0px 0px 2px 5px #000;",
		"}",
		"",
		"/*Pinned Title Floater*/",
		".pinnable-content.pinned,",
		".pinnable-content.pinned .link.odd,",
		".res-nightmode .pinnable-content.pinned,",
		".res-nightmode .pinnable-content.pinned .link.odd",
		"{",
		"    background: #262626 !important;",
		"    background-color:#262626!important;",
		"    border-radius: 4px!important;",
		"}",
		"",
		"",
		"",
		"/*Pinned Title Floater",
		".res-show-link,",
		".res-nightmode .res-show-link",
		"{",
		"    background: #000 !important;",
		"    border-radius: 4px!important;",
		"}",
		"",
		"/*Reddit Comment Hover Disable - Border Optional*/",
		"._1poyrkZ7g36PawDueRza-J,",
		"._2XDITKxlj4y3M99thqyCsO",
		"{",
		"    background-color: #0000!important;",
		"}",
		"",
		"._1g70Ndz7edo5LA6hie1Gkt,",
		"._2XDITKxlj4y3M99thqyCsO,",
		".listing-page .link",
		"{",
		"    transition: .2s;",
		"}",
		"._1g70Ndz7edo5LA6hie1Gkt:hover,",
		"._2XDITKxlj4y3M99thqyCsO:hover,",
		".listing-page .link:hover",
		"{",
		"    background-color: #0000!important;",
		"    box-shadow: 0 0 2px 0px #0000;",
		"    border-radius: 8px;",
		"    transition: delay !important;",
		"}",
		"/*._1g70Ndz7edo5LA6hie1Gkt",
		"{",
		"    background-color: #0000!important;",
		"    box-shadow: 0 0 2px 0px #0000;",
		"    transition: 1s;",
		"}*/",
		"/*",
		"/*Post Hover*/",
		"/*._2XDITKxlj4y3M99thqyCsO:hover",
		"{",
		"    background-color: #0000!important;",
		"    border-radius: 8px;",
		"    transition: ease-out .4s !important;",
		"}",
		"/*._2XDITKxlj4y3M99thqyCsO",
		"{",
		"    background-color: #00000047!important;",
		"    border-radius: 8px;",
		"    transition: ease-out 1.2s !important;",
		"}*/",
		"",
		"/*New Reddit Color Picker",
		"._1i-ndNsTQtNr82sMJ0renj",
		"",
		"{",
		"    background-color: #111 !important;",
		"    border-radius: 4px!important;",
		"    box-shadow: 0px 0px 3px 0px #555!important;",
		"    z-index:1;",
		"}",
		"",
		"._35Suh6FFPv_wB32f8XV32 ,",
		"._1oW-7sYYlQS4GeHmj1ubBp",
		"{",
		"    background: #0000 !important;",
		"    z-index:999;",
		"    transition: .4s !important;",
		"}",
		"",
		"",
		"/*Reddit Image RES Controls*/",
		".res-media-controls.res-media-controls-left.res-media-controls-top",
		"{",
		"    transition: ease-out .4s !important;",
		"}",
		".res-icon.gearIcon",
		"{",
		"    color: #000!important;",
		"    border-radius: 4px 0px 0px 4px;",
		"    box-shadow: 0px 0px 2px 0px #858585;",
		"}",
		".res-media-controls-rotate.res-media-controls-rotate-left.res-icon,",
		".res-media-controls-rotate.res-media-controls-rotate-right.res-icon,",
		".res-media-controls-download.res-icon,",
		".res-media-controls-lookup.res-icon",
		"{",
		"    box-shadow: 0px 0px 2px 0px #858585;",
		"}",
		".res-media-controls-clippy",
		"{",
		"    border-radius: 0px 4px 4px 0px;",
		"    box-shadow: 0px 0px 2px 0px #858585;",
		"}",
		"",
		"",
		"/*#111*/",
		"html,",
		"body,",
		"body > div,",
		"header,",
		"div[data-redditstyle],",
		"div[style^=\"left:\"] div[role=\"button\"],",
		"#header,",
		"#header .user,",
		"#lightbox,",
		"#hamburgers,",
		"#header-bottom-left,",
		"#header-bottom-right,",
		"#sr-header-area,",
		"#sr-header-area .sr-list,",
		"#sr-header-area .dropdown.srdrop,",
		"#sr-header-area .width-clip,",
		"#sr-more-link,",
		"#overlayScrollContainer,",
		"#SHORTCUT_FOCUSABLE_DIV > div:first-child,",
		".Art,",
		".desktop-onboarding__col_sign-up_image,",
		".hover-bubble,",
		".link.odd,",
		".modal-dialog,",
		".res-nightmode .desktop-onboarding__col_sign-up_image,",
		".res-nightmode body,",
		".res-nightmode .RESDialogSmall,",
		".RESDialogSmall,",
		".RESDialogSmall > h3,",
		".RESDropdownOptions,",
		".RESNotification,",
		".side h6:nth-of-type(1),",
		".side .md > ul:nth-of-type(1),",
		".side .md > ul:nth-of-type(1) li a,",
		".side .md h1,",
		".side .md h2,",
		".side .md h3,",
		".side .md h4,",
		".side .md h5,",
		".side .md h6,",
		".side .titlebox h1,",
		".titlebox .md h1,",
		".titlebox .md h2,",
		".titlebox .md h3,",
		".titlebox .md h4,",
		".titlebox .md h5,",
		".titlebox .md h6,",
		".titlebox blockquote ul li a,",
		".TMMvbwyeh9yuHKmQWHrE3,",
		"._197SqY-gDtObhzjz9SlSEp,",
		"._1AjUa6WOfWNHT6Hgt6TPmK,",
		"._1MHCyMQAMeqRqf5DPWWeq3,",
		"._1oYEKCssGFjqxQ9jJMNj5G,",
		"._1HSQGYlfPWzs40LP4_oFi5,",
		"._2KotRmn9DgdA58Ikji2mnV,",
		"._2R3RlhymCOkPrz9TusvcPq,",
		"._2v9AoSifv20X3GQbcYnxmn,",
		"._3DQXGvoE1SM3Kmz20of7Iz,",
		"._3RzIwmCawx9KWuyuEKa-3s,",
		"._3WeRItzSNTP8PlJhYPlcOV,",
		"._m7PpFuKATP9fZF4xKf9R,",
		".EjdBJNEwygtHMKiHd3Bnv,",
		".nSJCQrnLY07CzwT8tTsNO,",
		".RrvkJ7ntzKYyaCOxMJ1RM,",
		".tVQ1dB4n0mAWdcQNxFq-K",
		"{",
		"    background:#111!important;",
		"    background-color: #111 !important;",
		"}",
		"",
		"/*#111 fill w/borders*/",
		"body.combined-search-page.search-page,",
		"div.side a[href*=\"submit?sidebar\"],",
		"#alert_message .drop-choices,",
		"#RESNotifications,",
		"#RESNotification-0,",
		"#srList,",
		".author-tooltip__head,",
		".drop-choices.inuse,",
		".drop-choices.lightdrop,",
		".drop-choices.lightdrop.inuse,",
		".flairselector.drop-choices.active,",
		".res-nightmode .flairselector.drop-choices.active,",
		".read-next,",
		".res-commentNavToggle-choices,",
		".res-nightmode .read-next,",
		".res-nightmode .reddit-infobar,",
		".res-show-link,",
		".reddit-infobar,",
		".side #search #searchexpando,",
		".side .md [href*=\"q=flair:\"],",
		".side .sidecontentbox ul.content,",
		"._1IyDH2lB_YliAPREBY8LQQ,",
		"._1uYq_b7LvkEZrfcbgktTjV.qTijf5vcQE7xnXJmD6eDk,",
		"._2-aCCpAklEV0DkVWrpodjE,",
		"._3ks0MaOGG46svtYF11UgzA,",
		"._3RhWPJfjpsEoBw52x0fQp2,",
		"._3ZTxrJ36ejYA39ZNctzfrx,",
		".K4Eem3pMbRbAYioOfqN5E",
		"{",
		"    background: #111!important;",
		"    background-color: #111 !important;",
		"    border-radius: 4px!important;",
		"    box-shadow: 0px 0px 3px 0px #555!important;",
		"}",
		".RESDialogSmall.livePreview > h3",
		"{",
		"    background: #111!important;",
		"    background-color: #111 !important;",
		"    border-radius: 4px 4px 0px 0px!important;",
		"    box-shadow: 0px 0px 3px 0px #555!important;",
		"}",
		"",
		"",
		"/*Comment Fields for visitors*/",
		".comment,",
		".comment .comment,",
		".comment .comment .comment,",
		".comment .comment .comment .comment,",
		".comment .comment .comment .comment .comment,",
		".comment .comment .comment .comment .comment .comment,",
		".comment .comment .comment .comment .comment .comment .comment,",
		".comment .comment .comment .comment .comment .comment .comment .comment,",
		".comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-commentBoxes.res-commentBoxes-rounded,",
		".res-commentBoxes.res-commentBoxes-rounded .comment,",
		".res-commentBoxes.res-commentBoxes-rounded .comment .comment,",
		".res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment,",
		".res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment,",
		".res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment,",
		".res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment,",
		".res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment,",
		".res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded .comment,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded .comment .comment,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment,",
		".res-nightmode.res-commentBoxes.res-commentBoxes-rounded .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment .comment",
		"{",
		"",
		"    background-color: #111!important;",
		"    border: none !important;",
		"    border-radius: 4px;",
		"    z-index: 9999!important;",
		"}",
		"",
		"/*Misc smaller backgrounds*/",
		"/*RES Highlighting, Boxes & Commenmts*/",
		"/*Load More Comments Thingy*/",
		".entry.unvoted.selected",
		"{",
		"    background: #0000 !important;",
		"}",
		".md",
		"{",
		"    background: #0000 !important;",
		"    border-radius: 4px!important;",
		"}",
		".comment.noncollapsed > .entry > .usertext > .usertext-body",
		"{",
		"    background: #0000 !important;",
		"    border-radius: 4px!important;",
		"}",
		"",
		"/*Highlight Boxes*/",
		"/*Text Line only - Compatibility fix*/",
		".entry.res-selected .md p,",
		".entry.res-selected .md-container,",
		".res-nightmode .entry.res-selected .md-container",
		"{",
		"    background: #0000 !important;",
		"}",
		"/*Invisible Prepping*/",
		".RES-keyNav-activeElement,",
		".res-nightmode .entry.res-selected,",
		".link .usertext .md,",
		".RESNotifications,",
		".res-nightmode .RESNotification",
		"{",
		"    background: #0000 !important;",
		"    border-radius: 4px!important;",
		"}",
		"",
		"/*Main Highlightning*/",
		".entry.res-selected,",
		".res-nightmode .entry.res-selected,",
		".entry .unvoted .RES-keyNav-activeElement .res-selected",
		"{",
		"    background: #000 !important;",
		"    border-radius: 4px!important;",
		"    box-shadow: 0px 0px 2px 0px #333;",
		"}",
		"",
		"",
		"/*Comment Expander*/",
		".comment .expand",
		"{",
		"    background: #111 !important;",
		"    border-radius: 4px!important;",
		"}",
		".comment .expand:hover",
		"{",
		"    background: #000 !important;",
		"    border-radius: 4px!important;",
		"}",
		"",
		"",
		"/*RES Floater Upper Right Hand Corner*/",
		"#RESShortcutsEditContainer,",
		".res-floater-list,",
		".res-floater-visibleAfterScroll:hover,",
		".res-nightmode .res-floater-list,",
		".res-nightmode .res-floater-visibleAfterScroll:hover",
		"{",
		"    background-color: #000 !important;",
		"    border-radius: 5px!important;",
		"    transition: .4s;",
		"}",
		".res-wheel-browse:hover",
		"",
		"/*New Reddit Post/Reply Hover Floater*/",
		"/*Border Adjustments*/",
		"._324SLny5spJ682AiI73uBO,",
		"._324SLny5spJ682AiI73uBO:hover",
		"{",
		"    padding: 8px 0px 0px 8px !important;",
		"}",
		"/*Background Hover Fix*/",
		"._2mg6JuVCrcHWJkUQvZXFcZ,",
		"._2mg6JuVCrcHWJkUQvZXFcZ:hover",
		"{",
		"    padding-top: 20px;",
		"    padding-bottom: 0px;",
		"}",
		"",
		"/*RES Voting Floater*/",
		".res-floater-visibleAfterScroll:hover",
		"{",
		"    background: #151515 !important;",
		"    border-radius: 10px!important;",
		"    box-shadow: 0px 0px 3px 3px #111;",
		"}",
		".res-floater-visibleAfterScroll",
		"{",
		"    background: #161616 !important;",
		"    border-radius: 10px!important;",
		"    box-shadow: 0px 0px 15px 0px #151515;",
		"    transition: 1s !important;",
		"}",
		"",
		"",
		"/*#191919*/",
		"/* Inputs and buttons */",
		"input,",
		".login-form-side input,",
		"#search input[type=\"text\"]",
		"{",
		"    background: #191919 !important;",
		"    border: none !important;",
		"    font-weight: normal !important;",
		"    color: #fff !important;",
		"}",
		".pretty-form.short-text input.number[type=\"text\"]",
		"{",
		"    background: #0f0f0f !important;",
		"    border: 1px solid #333 !important;",
		"    border-radius: 3px !important;",
		"    margin: 0 0em;",
		"    font-family: courier !important;",
		"    color: #fff !important;",
		"    transition: .4s;",
		"}",
		".pretty-form.short-text input.number[type=\"text\"]:focus",
		"{",
		"    background: #000 !important;",
		"    box-shadow: 0px 0px 3px 0px #fff;",
		"    color: #ccc !important;",
		"    transition: .4s;",
		"}",
		"select",
		"{",
		"    background: #000 !important;",
		"    border: 1px solid #333 !important;",
		"    border-radius: 3px;",
		"    color: #ccc !important;",
		"}",
		"",
		"textarea,",
		".res-nightmode textarea,",
		"div[contenteditable=\"true\"],",
		"#infobar,",
		".infobar.listingsignupbar,",
		".linkinfo .shortlink input,",
		".new-comment .usertext-body,",
		".wikiaction-current,",
		"._2baJGEALPiEMZpWB2iWQs7",
		"{",
		"    background-color: #191919 !important;",
		"    background: #191919 !important;",
		"    color: #999 !important;",
		"    font-weight: normal !important;",
		"}",
		"textarea,",
		"input,",
		"input.number,",
		"._2baJGEALPiEMZpWB2iWQs7",
		"{",
		"    transition: .4s;",
		"}",
		"textarea:focus,",
		".res-nightmode textarea:focus,",
		"input:focus,",
		"input.number:focus,",
		"#search input[type=\"text\"]:focus",
		"{",
		"    background: #000 !important;",
		"    border-radius: 5px;",
		"    box-shadow: 0px 0px 3px 0px #333;",
		"    color: #ccc !important;",
		"    transition: .4s;",
		"}",
		"._2baJGEALPiEMZpWB2iWQs7 :focus",
		"{",
		"    background: #000 !important;",
		"    box-shadow: 0px 0px 3px 0px #333;",
		"    color: #ccc !important;",
		"    transition: .4s;",
		"}",
		"",
		"/*Home-Made Search Button on Selected Search Bars*/",
		"#search input[type=\"submit\"]",
		"{",
		"    background: #333 !important;",
		"    border-radius: 10px;",
		"    box-shadow: 0px 0px 0px 0px #555;",
		"    transition: .4s;",
		"}",
		"",
		"#search input[type=\"submit\"]:hover",
		"{",
		"    background: #7c892f !important;",
		"    border-radius: 10px;",
		"    box-shadow: 0px 0px 4px 3px #555;",
		"    color: #ccc !important;",
		"    transition: .4s;",
		"}",
		"",
		"/*#222 - Buttons Mainly*/",
		".blueButton:hover,",
		".morelink:hover,",
		"._1x6pySZ2CoUnAfsFhGe7J1:hover,",
		".fancy-toggle-button a:hover,",
		".usertext button:hover,",
		"button:hover,",
		".wiki-page .pageactions .wikiaction:hover,",
		".option.active.add.login-required,",
		".titlebox .md > ul:first-child li strong a",
		"{",
		"    background: #222 !important;",
		"    color: #ccc !important;",
		"    transition: .4s !important;",
		"}",
		"",
		"._1ekaLCxKd98FlrNC_Oo2rT,",
		".combined-search-page .search-page",
		"{",
		"    background-color: #222 !important;",
		"}",
		"/*#222 Selected for Transition (Buttons, mainly)*/",
		"._1YWXCINvcuU7nk0ED-bta8,",
		"._2KotRmn9DgdA58Ikji2mnV,",
		"._2MgAHlPDdKvXiG-Qbz5cbC,",
		"._3iLNO7R8VMvJ4Dn89SVFVg,",
		"._3LwUIE7yX7CZQKmD2L87vf,",
		"._3oyS3dPRsa51zDEONlIdts,",
		"._3X4hbg4asgVvLaVYU6dUzs,",
		"._6Ej82J4aTDK36LLOcpFbC,",
		"._3dZnYgFFpifT-M_Vs2FAq6,",
		".PH-V9ggsF2mi5JTDmDqdR,",
		".RESCloseButton,",
		"input[type=\"submit\"]",
		"{",
		"    transition: .4s ease-in-out !important;",
		"}",
		"._1HunhFR-0b-AYs0WG9mU_P:hover,",
		"._1YWXCINvcuU7nk0ED-bta8:focus,",
		"._1YWXCINvcuU7nk0ED-bta8:hover,",
		"._2KotRmn9DgdA58Ikji2mnV:hover,",
		"._2MgAHlPDdKvXiG-Qbz5cbC:hover,",
		"._2nelDm85zKKmuD94NequP0:hover,",
		"._3iLNO7R8VMvJ4Dn89SVFVg:hover,",
		"._3LwUIE7yX7CZQKmD2L87vf:hover,",
		"._3oyS3dPRsa51zDEONlIdts:hover,",
		"._3UhcBirLMXHSbN5vYYBGSF:hover,",
		"._3X4hbg4asgVvLaVYU6dUzs:hover,",
		"._3dZnYgFFpifT-M_Vs2FAq6:hover,",
		"._3K5j589m2GGg7J3pR96mQf,",
		".PH-V9ggsF2mi5JTDmDqdR:hover,",
		".RESCloseButton:hover,",
		".wLV79_wV-ziNiWmf3Y7OV._33qmTi-steAtfFkONvaiX3:hover,",
		".wLV79_wV-ziNiWmf3Y7OV.j5ti73p7EAjBJvJlSjppe,",
		".option.active.add.login-required,",
		".helplink.access-required:hover,",
		"input[type=\"submit\"]:hover,",
		".side .md > ul:nth-of-type(1) li a:hover",
		"{",
		"    background-color: #222 !important;",
		"    transition: .4s ease-in-out !important;",
		"}",
		"",
		"",
		"/*#333*/",
		"#RESConsoleContainer,",
		"#RESShortcutsAddFormContainer,",
		".guider,",
		".guiders_button",
		"{",
		"    background: #333 !important;",
		"}",
		".res-nightmode .res-fancy-toggle-button,",
		".res-fancy-toggle-button,",
		".option.active.add.login-required:hover,",
		".titlebox .md > ul:first-child li strong a:hover,",
		".titlebox .RESDashboardToggle,",
		".titlebox .RESshortcutside,",
		".wLV79_wV-ziNiWmf3Y7OV.j5ti73p7EAjBJvJlSjppe:hover,",
		"._3K5j589m2GGg7J3pR96mQf:hover",
		"{",
		"    background: #333 !important;",
		"    transition: .4s;",
		"}",
		"/*Tiny \"+\" button next to subreddit link in Title*/",
		"._2zcGm9WDxG67GYyNNvHzlA",
		"{",
		"    background: #444;",
		"    border-radius:10px;",
		"    transition: .4s;",
		"}",
		"._2zcGm9WDxG67GYyNNvHzlA:hover",
		"{",
		"    background: #666;",
		"    border-radius:10px;",
		"    transition: .4s;",
		"}",
		"",
		"",
		"/*#555*/",
		".res-fancy-toggle-button:hover",
		"{",
		"    background: #555 !important;",
		"    transition: .4s;",
		"}",
		"",
		"",
		"/*Borders*/",
		"/*Popups and selected Section Borders*/",
		"#alert_message,",
		".side,",
		".author-tooltip.hover-bubble.author-tooltip_common.anchor-bottom-left,",
		".hover-bubble.anchor-right,",
		".RESDialogSmall,",
		".RESDropdownOptions,",
		".RESNotification,",
		".RrvkJ7ntzKYyaCOxMJ1RM,",
		"._197SqY-gDtObhzjz9SlSEp,",
		"/*Main Dropdown:*/",
		"._1HSQGYlfPWzs40LP4_oFi5,",
		"._1FUNcfOeszr8eruqLxCMcR,",
		"._1MHCyMQAMeqRqf5DPWWeq3,",
		"._m7PpFuKATP9fZF4xKf9R,",
		".EjdBJNEwygtHMKiHd3Bnv,",
		".nSJCQrnLY07CzwT8tTsNO,",
		".TMMvbwyeh9yuHKmQWHrE3",
		"{",
		"    border-radius: 4px 4px 4px 4px!important;",
		"    box-shadow: 0px 0px 3px 0px #555;",
		"}",
		"",
		"/*Comment Divider*/",
		"hr,",
		".res-nightmode hr,",
		".res-nightmode .md hr",
		"{",
		"    height: 1.3px;",
		"    border-color: #3c3c3c;",
		"    background-color: #3c3c3c;",
		"}",
		"",
		"/* Top bar */",
		"#header-img,",
		"#mail,",
		".nohavemail,",
		".sidebox.create .spacer a",
		"{",
		"    filter: invert(80%) hue-rotate(180deg) !important;",
		"}",
		"",
		"/* Hide subreddits top bar */",
		"/*[[sr-top-bar]]*/",
		".sr-list > .separator,",
		".sr-list > .separator + .sr-bar,",
		"#sr-more-link",
		"{",
		"    display: none !important;",
		"}",
		"",
		"#sr-header-area",
		"{",
		"    height: 10 !important;",
		"}",
		"",
		".sr-bar,",
		".dropdown.srdrop",
		"{",
		"    float: left !important;",
		"    margin-left: 0.5em;",
		"}",
		"",
		"/*Sub Tabs*/",
		".tabmenu li.res-tabmenu-button a,",
		".tabmenu .selected a",
		"{",
		"    background: #0000!important;",
		"    color: #ccc!important;",
		"    transition: .4s;",
		"}",
		"",
		".tabmenu li a",
		"{",
		"    background-color: #111 !important;",
		"    transition: .4s;",
		"}",
		"",
		"/*li.tabmenu.selected, .choice.primary,",
		".tabmenu .selected a",
		"{",
		"    background: #191919 !important;",
		"}*/",
		"/*Chat*/",
		"/*Main Backgrounds*/",
		".RaDn8d65cHW3dCPgTkSRN,",
		"#chat-container,",
		".RaDn8d65cHW3dCPgTkSRN,",
		".tVQ1dB4n0mAWdcQNxFq-K,",
		"._2ERphYf08pZHF6WWqsLCvA,",
		"._1H6XmzDZeFebELnU6sY0C0,",
		"._3mRkaGP5f0vxRe_U_Gy02A",
		"{",
		"    background-color: #000 !important;",
		"    transition: all .4s !important;",
		"}",
		"",
		"/*Chat Loading Screen*/",
		"._2zstkXjlzum5VrIXwJ_LbE",
		"{",
		"    background-color: #000 !important;",
		"    transition: all .4s !important;",
		"}",
		"/*Chat Loading Screen Spinner*/",
		"._1o-ux8CJI01lsz3U88wukN",
		"{",
		"    background-color: #0000 !important;",
		"    transition: all .4s !important;",
		"}",
		"",
		"/*\"Looking for internet\" message*/",
		".Z2zpHoQtb1-SNgOtRrt8J",
		"{",
		"    background-color: #000 !important;",
		"    box-shadow: 0px 0px 3px 0px #555;",
		"}",
		"/*Borders*/",
		"._1qhSxdEj1WQNv1ToBDZ2SG,",
		"._1FE1o9C7Gk8k6ciF8-oeXx",
		"{",
		"    box-shadow: 0px 0px 3px 0px #555;",
		"}",
		"/*Active Chat Highlight*/",
		"._1kLrU4-nGav42QWAbioHOC,",
		"._3X4hbg4asgVvLaVYU6dUzs:hover",
		"{",
		"    background-color: #0000 !important;",
		"    box-shadow: 0px 0px 2px 0px #555;",
		"}",
		"",
		"/*Chat Channel Hover*/",
		"._1uYq_b7LvkEZrfcbgktTjV:hover,",
		"._3X4hbg4asgVvLaVYU6dUzs:hover",
		"{",
		"    background-color: #0000 !important;",
		"    box-shadow: 0px 0px 2px 0px #fff;",
		"}",
		"",
		"/* Texts Hover*/",
		"._1QLRzzdhJM_LEu2w38yEOx,",
		"._3bzYm5d0EW5ZCDVM6kmYIh",
		"{",
		"    transition: .4s !important",
		"}",
		"._1QLRzzdhJM_LEu2w38yEOx:hover,",
		"._3bzYm5d0EW5ZCDVM6kmYIh:hover,",
		".option.active.add.login-required:hover",
		"{",
		"    color: #fff !important;",
		"    transition: .4s !important",
		"}",
		"",
		"",
		"/*Text Input*/",
		".TqpfKgK2FdKbljZzdRLIU",
		"{",
		"    background-color: #000 !important;",
		"    box-shadow: 0px 0px 2px 0px #fff;",
		"    color: #fff!important;",
		"}",
		"",
		"/*Spoiler Fix*/",
		"/*Spoiler Indicator, Left*/",
		"blockquote,",
		".md-spoiler-text,",
		".md blockquote",
		"{",
		"    border-left: solid 4px #444 !important;",
		"    background-color:#111;",
		"}",
		"/*Spoiler Cover Color*/",
		".md .md-spoiler-text/*:not(.notrevealed)*/",
		"",
		"{",
		"    background: #222 !important;",
		"    transition: 1s;",
		"}",
		"/*Spoiler Cover After*/",
		".md .md-spoiler-text.revealed",
		"{",
		"    background: #55555525 !important;",
		"    box-shadow: 0 0 5px 0 #55555525;",
		"    transition: 1s;",
		"}",
		"/* Spoiler Fix - Word >!...!< */",
		"/*Before*/",
		"span.md-spoiler-text:not(.revealed),",
		".res-nightmode .md .md-spoiler-text:not(.revealed)",
		"{",
		"    color: #0000 !important;",
		"    transition: 1s!important;",
		"}",
		"/*Word Text After*/",
		".md .md-spoiler-text",
		"{",
		"    color: #ccc !important;",
		"    transition: 1s!important;",
		"}",
		"",
		"/*Word Text After - RES Nighmode*/",
		".res-nightmode .entry.res-selected .md-container > .md",
		"{",
		"    color: #aaa !important;",
		"    transition: 1s!important;",
		"}",
		"",
		"/* Voting Buttons, Arrows & Misc Colors */",
		".voteButton,",
		".voteButton:hover,",
		"._2UH-_HmB6X-2cCsOZVDtC5:hover",
		"{",
		"    background-color: transparent !important;",
		"}",
		"/* Toggle Buttons Background On*/",
		"._2e2g485kpErHhJQUiyvvC2._1L5kUnhRYhUJ4TkMbOTKkI",
		"{",
		"    background-color: #666 !important;",
		"}",
		"/* Toggle Button Heads */",
		"._2FKpII1jz0h6xCAw1kQAvS",
		"{",
		"    background-color: #888 !important;",
		"}",
		"",
		"/* Reply Editor Buttons - Markdown */",
		".edit-btn",
		"{",
		"    background-color: #0000 !important;",
		"    filter: invert(50%);",
		"}",
		".edit-btn:hover",
		"{",
		"    background-color: #0000 !important;",
		"    filter: invert(80%);",
		"}",
		"",
		"/* Hide annoying stuff */",
		"/*.give-gold-button,*/",
		"/*.goldvertisement,*/",
		"/*.embed-comment,*/",
		".nub,",
		".threadline,",
		"div.side [href*=\"submit?sidebar\"]::before",
		"{",
		"    display: none !important;",
		"}",
		"",
		"",
		"/* Flairs, Tags, RES User Tags Indicators*/",
		".flair,",
		".res-nightmode .flair",
		"{",
		"    color: #aaa!important;",
		"    padding: 0 4px;",
		"    background-color: inherit!important;",
		"    filter: brightness(80%)!important;",
		"    border: 1px solid #c7c7c780!important;",
		"    border-radius: 4px;",
		"    font-size: 0.85em;",
		"    transition: .4s;",
		"}",
		"",
		"/*Post Tag*/",
		".flairrichtext,",
		".linkflairlabel,",
		".linkflair-Doggo .linkflairlabel,",
		".linkflair-Template .linkflairlabel,",
		".linkflair-News .linkflairlabel,",
		".res-flairSearch.linkflairlabel > a,",
		".res-nightmode .flairrichtext,",
		".res-nightmode .linkflairlabel,",
		".res-nightmode .linkflair-Doggo .linkflairlabel,",
		".res-nightmode .linkflair-Template .linkflairlabel,",
		".res-nightmode .linkflair-News .linkflairlabel,",
		".res-nightmode .res-flairSearch.linkflairlabel > a",
		"{",
		"    font-weight: 600;",
		"    font-size: .75em;",
		"    color: inherit!important;",
		"    padding: 0px 5px;",
		"    filter: brightness(80%)!important;",
		"    border: 1px solid #c7c7c780!important;",
		"    border-radius: 4px;",
		"    transition: .4s;",
		"}",
		"",
		"/*RES User Tag Indicator*/",
		".RESUserTagImage::after",
		"{",
		"    color: #aaa!important;",
		"}",
		".res-nightmode .RESUserTagImage::after",
		"{",
		"    filter: brightness(80%);",
		"}",
		"",
		"/*RES USer Tags*/",
		".userTagLink.hasTag.truncateTag,",
		".userTagLink.hasTag",
		"{",
		"    filter: brightness(80%)/*opacity(80%) saturate(80%)*/",
		"    !important;",
		"    border: 1px solid #c7c7c780!important;",
		"    border-radius: 4px;",
		"    transition: .4s;",
		"}",
		".res-nightmode .side .RESUserTag.userTagLink.hasTag.truncateTag,",
		".res-nightmode .userTagLink.hasTag.truncateTag,",
		".res-nightmode .userTagLink.hasTag",
		"{",
		"    filter: brightness(60%)/*opacity(80%) saturate(80%)*/",
		"    !important;",
		"    border: 1px solid #c7c7c780!important;",
		"    border-radius: 4px;",
		"    transition: .4s;",
		"}",
		"",
		".res-nightmode .RESUserTag:hover,",
		".RESUserTag:hover,",
		".res-nightmode .userTagLink.hasTag:hover,",
		".userTagLink.hasTag:hover,",
		".userTagLink.hasTag.truncateTag:hover,",
		".hasTag:hover",
		"{",
		"    filter: brightness(100%)/*opacity(80%) saturate(80%)*/",
		"    !important;",
		"    border-radius: 4px;",
		"    transition: .4s;",
		"}",
		"",
		"/*Admin Highlight*/",
		".icon-admin",
		"{",
		"    color: #f008 !important;",
		"}",
		".admin",
		"{",
		"    color: #a66 !important;",
		"}",
		"",
		"",
		"/*OP Highlight*/",
		"",
		"._zMIUk6t-WDI7fxfkvD02,",
		".author.submitter,",
		".res-nightmode .author.submitter",
		"",
		"{",
		"    color: #ccc!important;",
		"    background-color: #00f!important;",
		"    padding-left: .2pc;",
		"    padding-right: 0.2pc;",
		"    filter: brightness(70%);",
		"    transition: .4s;",
		"}",
		"._zMIUk6t-WDI7fxfkvD02:hover,",
		".author.submitter:hover,",
		".res-nightmode .author.submitter:hover",
		"{",
		"    color: #fff!important;",
		"    background-color: #00f!important;",
		"    padding-left: .2pc;",
		"    padding-right: 0.2pc;",
		"    filter: brightness(100%);",
		"    transition: .4s;",
		"}",
		"",
		"/*Moderator*/",
		".tagline .moderator,",
		".thing .tagline .author.moderator",
		"{",
		"    color: #0f0!important;",
		"    background-color: #001900 !important;",
		"    border: 1px solid #00c44280!important;",
		"    border-radius: 4px;",
		"    padding-left: .15pc;",
		"    padding-right: .15pc;",
		"    box-shadow: 0px 0px 0px 0px;",
		"    filter: brightness(80%);",
		"    transition: .4s;",
		"}",
		".tagline .moderator:hover,",
		".thing .tagline .author.moderator:hover",
		"{",
		"    filter: brightness(100%);",
		"}",
		".res-nightmode .tagline .moderator,",
		".res-nightmode .thing .tagline .author.moderator",
		"{",
		"    filter: brightness(70%);",
		"}",
		".res-nightmode .tagline .moderator:hover,",
		".res-nightmode .thing .tagline .author.moderator:hover",
		"{",
		"    filter: brightness(80%);",
		"}",
		"._1vh4u-B3eHXE3-ko0huS20",
		"{",
		"    background-color: #0f08 !important;",
		"}",
		"",
		"/*Sticky & Announcement*/",
		".linkflair-News",
		"{",
		"    background-color: #001900 !important;",
		"}",
		"/*Subreddit Themed Stickie-Background Removal*/",
		".listing-page .odd.stickied",
		"{",
		"    background-image: none !important;",
		"}",
		"",
		".green,",
		".stickied-tagline,",
		".trending-subreddits strong",
		"{",
		"    color: #3a3 !important;",
		"}",
		"",
		"/*Spoiler Warning*/",
		":root",
		"{",
		"    --spoiler: #a65e0d !important;",
		"}",
		".spoiler-stamp,",
		".res-nightmode .spoiler-stamp",
		"{",
		"    background: #000!important;",
		"    color: var(--spoiler) !important;",
		"    border: 1px solid var(--spoiler) !important;",
		"    border-radius: 4px;",
		"    padding-left: 0.1pc;",
		"    padding-right: 0.1pc;",
		"    filter: brightness(80%);",
		"    transition: .4s;",
		"}",
		".spoiler-stamp:hover,",
		".res-nightmode .spoiler-stamp:hover",
		"{",
		"    filter: brightness(100%);",
		"    transition: .4s;",
		"}",
		"",
		"/*NSFW Warning*/",
		":root",
		"{",
		"    --nsfw: #d10023 !important;",
		"}",
		".nsfw-stamp acronym,",
		".res-nightmode .nsfw-stamp acronym",
		"{",
		"    background: #000!important;",
		"    color: var(--nsfw) !important;",
		"    border: 1px solid var(--nsfw) !important;",
		"    border-radius: 4px;",
		"    padding-left: 0.1pc;",
		"    padding-right: 0.1pc;",
		"    filter: brightness(65%);",
		"    transition: .4s;",
		"}",
		".nsfw-stamp acronym:hover,",
		".res-nightmode .nsfw-stamp acronym:hover",
		"{",
		"    filter: brightness(100%);",
		"    transition: .4s;",
		"}",
		"",
		"",
		"/*Deleted Posts*/",
		".thing.spam",
		"{",
		"    background-color: #200 !important;",
		"}",
		"",
		"/*Non-Subscriber Register Notice*/",
		"body:not(.subscriber) #siteTable::before",
		"{",
		"    color: #ccc;",
		"    text-align: center;",
		"    padding-left: 0px;",
		"    font-size: 1.2em;",
		"    background-color: #000 !important;",
		"    /*border: 1px solid #000!important;*/",
		"    box-shadow: 0px 0px 3px 0px #555;",
		"    border-radius: 20px;",
		"}",
		"",
		"/*Preferences - RSS feeds - Buttons Treatment*/",
		".private-feeds.instructions .feedlink",
		"{",
		"    filter: opacity(75%) !important;",
		"}",
		".private-feeds.instructions .feedlink:hover",
		"{",
		"    filter: opacity(100%) !important;",
		"}",
		"",
		"/* Side bar */",
		".listing-chooser-collapsed .grippy",
		"{",
		"    width: 1px !important;",
		"}",
		"",
		".listing-chooser-collapsed .listing-chooser",
		"{",
		"    padding-right: 1px !important;",
		"}",
		"",
		".listing-chooser li.selected",
		"{",
		"    box-shadow: none !important;",
		"}",
		"",
		".grippy:after",
		"{",
		"    display: none !important;",
		"}",
		"/*Logo + Icons Treatment*/",
		"#header svg,",
		".Header__icon",
		"{",
		"    filter: invert(90%) hue-rotate(180deg);",
		"}",
		"",
		"svg",
		"{",
		"    filter: saturate(.8) brightness(.8) contrast(.8) !important;",
		"    transition: .4s !important;",
		"}",
		"svg:after,",
		"svg:focus,",
		"svg:hover",
		"{",
		"    filter: saturate(1) brightness(1) contrast(1) !important;",
		"    transition: .4s !important;",
		"}",
		"",
		"/*New Reddit Logo*/",
		"svg._1bWuGs_1sq4Pqy099x_yy-",
		"{",
		"    filter: brightness(.0);",
		"        transition: .4s;",
		"}",
		"svg._1bWuGs_1sq4Pqy099x_yy-:hover,",
		"._1bWuGs_1sq4Pqy099x_yy- g:hover,",
		"._1bWuGs_1sq4Pqy099x_yy- g path",
		"{",
		"    filter: brightness(2.8)!important;",
		"    transition: .4s;",
		"}",
		"",
		".arrow",
		"{",
		"    filter: brightness(45%);",
		"}",
		"",
		".arrow.upmod,",
		".arrow.downmod",
		"{",
		"    filter: brightness(50%) contrast(120%);",
		"}",
		"",
		".link .score.likes",
		"{",
		"    color: #f77 !important;",
		"}",
		"",
		".link .score.dislikes",
		"{",
		"    color: #7070c2 !important;",
		"}",
		"",
		".expando-button,",
		".expando-button:hover,",
		".expando-button.collapsed.selftext,",
		".expando-button.collapsed.selftext:hover",
		"{",
		"    filter: brightness(55%) contrast(180%);",
		"    background-color: transparent !important;",
		"    transition:0s!important;",
		"}",
		"",
		"",
		"/* RES Voting Arrows */",
		".res-nightmode .arrow",
		"{",
		"    filter: none !important;",
		"}",
		"",
		".RESLoadingSpinner",
		"{",
		"    background: #999 !important;",
		"}",
		"",
		".search-expando.collapsed:before,",
		".comment-fade",
		"{",
		"    display: none !important;",
		"}",
		"",
		".recommended-link",
		"{",
		"    border-color: #333 !important;",
		"}",
		"/* Comments */",
		".gold-accent",
		"{",
		"    background-color: #11111152 !important;",
		"}",
		"",
		".md td",
		"{",
		"    border: solid 1px #333 !important;",
		"}",
		"",
		".comment .author,",
		".morecomments a",
		"{",
		"    font-weight: normal !important;",
		"}",
		"",
		"#overlayFixedContent + div",
		"{",
		"    background: rgba(0, 0, 0, 0.7) !important;",
		"}",
		"",
		"div[data-slot]",
		"{",
		"    display: none !important;",
		"}",
		"",
		"/*Reddit Gif Player*/",
		"/*Native Player Controls Background - Darker than vanilla*/",
		".reddit-video-controller-root.playback-controls",
		"{",
		"    background: linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, rgb(0, 0, 0) 100%) !important;",
		"    transition: ease-out .4s !important;",
		"}",
		"._3YDPJHFl8YQG4TIAGQwHwK",
		"{",
		"    background: linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, rgb(0, 0, 0) 100%) !important;",
		"    transition: ease-out .15s !important;",
		"}",
		"/*Volume Container*/",
		".volume-slider-container,",
		"._3nTkqMMtsoqxVmhplDRef3,",
		"/*Options Menu*/",
		".submenu,",
		".video-settings-container,",
		"._1s7GuSZPqjgn0QP60a6asr,",
		"/*Slidebar Preview Thumb Background*/",
		".seek-bar-floating,",
		".seek-bar-time,",
		".AZ2rAoFxu6aiCBbBq02Sr",
		"{",
		"    background: #000000d6 !important;",
		"    border-radius: 4px;",
		"}",
		"/*Buttons Hover-background removal - new Reddit*/",
		"._3QOPLw8PZipW3i8kDxod81:not(.using-keys) button:hover",
		"{",
		"    background: #0000 !important;",
		"}",
		"/*Volume Button*/",
		".volume-slider-thumb,",
		"._320cReGqgLhY1pPA-fM8Z7",
		"{",
		"    background: #ffffffd6 !important;",
		"}",
		"/*Volume Slider*/",
		".volume-slider-track,",
		".gzyVgIvE9b8wMmHefFf6i",
		"{",
		"    background: #ffffff36 !important;",
		"}",
		"/*Loading/Buffer Indicator*/",
		".seek-bar-buffered,",
		".seek-bar-lookahead,",
		"._4UI_04IgDx06P4biEkiF3",
		"{",
		"    background: #ffffff6b !important;",
		"}",
		"/*Play Progress Indicator*/",
		".seek-bar-progress,",
		"._3fnsfWuIyofBJBBCbeOZzM",
		"{",
		"    background: #fff !important;",
		"}",
		"/*old.reddit Play Progress Button Removal*/",
		".seek-bar-thumb.seek-bar-progress",
		"{",
		"    display: none !important;",
		"}",
		"",
		"/*RES Gif Player - Streamable*/",
		"/*Player Controls Background*/",
		".res-video-interface",
		"{",
		"    background: linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, rgb(0, 0, 0) 100%) !important;",
		"}",
		"/*Progress Bar*/",
		".res-video-progress",
		"{",
		"    background: #0000006d !important;",
		"    height: 3px !important;",
		"    padding: 0px !important;",
		"}",
		"/*Progres Indicator*/",
		"/*Color*/",
		".res-video-position",
		"{",
		"    background: #f00 !important;",
		"}",
		"/*Progres Indicator Hover Button*/",
		".res-video-position-thumb",
		"{",
		"    background: #ffffff80 !important;",
		"}",
		"/*Volume Control Background*/",
		".res-video-volume-level",
		"{",
		"    background: #00000080 !important;",
		"}",
		"/*Volume Indicator*/",
		".res-video-volume-percentage",
		"{",
		"    background: #ffffff80 !important;",
		"}"
	].join("\n");
if (false || (document.domain == "www.reddit.com" || document.domain.substring(document.domain.indexOf(".www.reddit.com") + 1) == "www.reddit.com") || (document.domain == "old.reddit.com" || document.domain.substring(document.domain.indexOf(".old.reddit.com") + 1) == "old.reddit.com") || (document.domain == "np.reddit.com" || document.domain.substring(document.domain.indexOf(".np.reddit.com") + 1) == "np.reddit.com") || (document.domain == "snew.notabug.io" || document.domain.substring(document.domain.indexOf(".snew.notabug.io") + 1) == "snew.notabug.io") || (document.domain == "notabug.io" || document.domain.substring(document.domain.indexOf(".notabug.io") + 1) == "notabug.io") || (document.domain == "af.reddit.com" || document.domain.substring(document.domain.indexOf(".af.reddit.com") + 1) == "af.reddit.com") || (document.domain == "ar.reddit.com" || document.domain.substring(document.domain.indexOf(".ar.reddit.com") + 1) == "ar.reddit.com") || (document.domain == "be.reddit.com" || document.domain.substring(document.domain.indexOf(".be.reddit.com") + 1) == "be.reddit.com") || (document.domain == "bg.reddit.com" || document.domain.substring(document.domain.indexOf(".bg.reddit.com") + 1) == "bg.reddit.com") || (document.domain == "bn-IN.reddit.com" || document.domain.substring(document.domain.indexOf(".bn-IN.reddit.com") + 1) == "bn-IN.reddit.com") || (document.domain == "bn.bd.reddit.com" || document.domain.substring(document.domain.indexOf(".bn.bd.reddit.com") + 1) == "bn.bd.reddit.com") || (document.domain == "bs.reddit.comx" || document.domain.substring(document.domain.indexOf(".bs.reddit.comx") + 1) == "bs.reddit.comx") || (document.domain == "ca.reddit.com" || document.domain.substring(document.domain.indexOf(".ca.reddit.com") + 1) == "ca.reddit.com") || (document.domain == "cs.reddit.com" || document.domain.substring(document.domain.indexOf(".cs.reddit.com") + 1) == "cs.reddit.com") || (document.domain == "cy.reddit.com" || document.domain.substring(document.domain.indexOf(".cy.reddit.com") + 1) == "cy.reddit.com") || (document.domain == "da.reddit.com" || document.domain.substring(document.domain.indexOf(".da.reddit.com") + 1) == "da.reddit.com") || (document.domain == "dd.reddit.com" || document.domain.substring(document.domain.indexOf(".dd.reddit.com") + 1) == "dd.reddit.com") || (document.domain == "de.reddit.com" || document.domain.substring(document.domain.indexOf(".de.reddit.com") + 1) == "de.reddit.com") || (document.domain == "ds.reddit.com" || document.domain.substring(document.domain.indexOf(".ds.reddit.com") + 1) == "ds.reddit.com") || (document.domain == "el.reddit.com" || document.domain.substring(document.domain.indexOf(".el.reddit.com") + 1) == "el.reddit.com") || (document.domain == "en.reddit.com" || document.domain.substring(document.domain.indexOf(".en.reddit.com") + 1) == "en.reddit.com") || (document.domain == "en.au.reddit.com" || document.domain.substring(document.domain.indexOf(".en.au.reddit.com") + 1) == "en.au.reddit.com") || (document.domain == "en.ca.reddit.com" || document.domain.substring(document.domain.indexOf(".en.ca.reddit.com") + 1) == "en.ca.reddit.com") || (document.domain == "en.gb.reddit.com" || document.domain.substring(document.domain.indexOf(".en.gb.reddit.com") + 1) == "en.gb.reddit.com") || (document.domain == "en.us.reddit.com" || document.domain.substring(document.domain.indexOf(".en.us.reddit.com") + 1) == "en.us.reddit.com") || (document.domain == "eo.reddit.com" || document.domain.substring(document.domain.indexOf(".eo.reddit.com") + 1) == "eo.reddit.com") || (document.domain == "es.reddit.com" || document.domain.substring(document.domain.indexOf(".es.reddit.com") + 1) == "es.reddit.com") || (document.domain == "es-ar.reddit.com" || document.domain.substring(document.domain.indexOf(".es-ar.reddit.com") + 1) == "es-ar.reddit.com") || (document.domain == "es-cl.reddit.com" || document.domain.substring(document.domain.indexOf(".es-cl.reddit.com") + 1) == "es-cl.reddit.com") || (document.domain == "es-mx.reddit.com" || document.domain.substring(document.domain.indexOf(".es-mx.reddit.com") + 1) == "es-mx.reddit.com") || (document.domain == "et.reddit.com" || document.domain.substring(document.domain.indexOf(".et.reddit.com") + 1) == "et.reddit.com") || (document.domain == "eu.reddit.com" || document.domain.substring(document.domain.indexOf(".eu.reddit.com") + 1) == "eu.reddit.com") || (document.domain == "fa.reddit.com" || document.domain.substring(document.domain.indexOf(".fa.reddit.com") + 1) == "fa.reddit.com") || (document.domain == "fi.reddit.com" || document.domain.substring(document.domain.indexOf(".fi.reddit.com") + 1) == "fi.reddit.com") || (document.domain == "fil.reddit.com" || document.domain.substring(document.domain.indexOf(".fil.reddit.com") + 1) == "fil.reddit.com") || (document.domain == "fr.reddit.com.reddit.com" || document.domain.substring(document.domain.indexOf(".fr.reddit.com.reddit.com") + 1) == "fr.reddit.com.reddit.com") || (document.domain == "fr.ca.reddit.com" || document.domain.substring(document.domain.indexOf(".fr.ca.reddit.com") + 1) == "fr.ca.reddit.com") || (document.domain == "fy-NL.reddit.com" || document.domain.substring(document.domain.indexOf(".fy-NL.reddit.com") + 1) == "fy-NL.reddit.com") || (document.domain == "ga-ie.reddit.com" || document.domain.substring(document.domain.indexOf(".ga-ie.reddit.com") + 1) == "ga-ie.reddit.com") || (document.domain == "gd.reddit.com" || document.domain.substring(document.domain.indexOf(".gd.reddit.com") + 1) == "gd.reddit.com") || (document.domain == "gl.reddit.com" || document.domain.substring(document.domain.indexOf(".gl.reddit.com") + 1) == "gl.reddit.com") || (document.domain == "he.reddit.com" || document.domain.substring(document.domain.indexOf(".he.reddit.com") + 1) == "he.reddit.com") || (document.domain == "hi.reddit.com" || document.domain.substring(document.domain.indexOf(".hi.reddit.com") + 1) == "hi.reddit.com") || (document.domain == "hr.reddit.com" || document.domain.substring(document.domain.indexOf(".hr.reddit.com") + 1) == "hr.reddit.com") || (document.domain == "hu.reddit.com" || document.domain.substring(document.domain.indexOf(".hu.reddit.com") + 1) == "hu.reddit.com") || (document.domain == "hy.reddit.com" || document.domain.substring(document.domain.indexOf(".hy.reddit.com") + 1) == "hy.reddit.com") || (document.domain == "id.reddit.com" || document.domain.substring(document.domain.indexOf(".id.reddit.com") + 1) == "id.reddit.com") || (document.domain == "is.reddit.com" || document.domain.substring(document.domain.indexOf(".is.reddit.com") + 1) == "is.reddit.com") || (document.domain == "it.reddit.com" || document.domain.substring(document.domain.indexOf(".it.reddit.com") + 1) == "it.reddit.com") || (document.domain == "ja.reddit.com" || document.domain.substring(document.domain.indexOf(".ja.reddit.com") + 1) == "ja.reddit.com") || (document.domain == "kn_IN.reddit.com" || document.domain.substring(document.domain.indexOf(".kn_IN.reddit.com") + 1) == "kn_IN.reddit.com") || (document.domain == "ko.reddit.com" || document.domain.substring(document.domain.indexOf(".ko.reddit.com") + 1) == "ko.reddit.com") || (document.domain == "la.reddit.com" || document.domain.substring(document.domain.indexOf(".la.reddit.com") + 1) == "la.reddit.com") || (document.domain == "leet.reddit.com" || document.domain.substring(document.domain.indexOf(".leet.reddit.com") + 1) == "leet.reddit.com") || (document.domain == "lol.reddit.com" || document.domain.substring(document.domain.indexOf(".lol.reddit.com") + 1) == "lol.reddit.com") || (document.domain == "lt.reddit.com" || document.domain.substring(document.domain.indexOf(".lt.reddit.com") + 1) == "lt.reddit.com") || (document.domain == "lv.reddit.com" || document.domain.substring(document.domain.indexOf(".lv.reddit.com") + 1) == "lv.reddit.com") || (document.domain == "mod.reddit.com" || document.domain.substring(document.domain.indexOf(".mod.reddit.com") + 1) == "mod.reddit.com") || (document.domain == "ms.reddit.com" || document.domain.substring(document.domain.indexOf(".ms.reddit.com") + 1) == "ms.reddit.com") || (document.domain == "mt-MT.reddit.com" || document.domain.substring(document.domain.indexOf(".mt-MT.reddit.com") + 1) == "mt-MT.reddit.com") || (document.domain == "nl.reddit.com" || document.domain.substring(document.domain.indexOf(".nl.reddit.com") + 1) == "nl.reddit.com") || (document.domain == "nn.reddit.com" || document.domain.substring(document.domain.indexOf(".nn.reddit.com") + 1) == "nn.reddit.com") || (document.domain == "no.reddit.com" || document.domain.substring(document.domain.indexOf(".no.reddit.com") + 1) == "no.reddit.com") || (document.domain == "oauth.reddit.com" || document.domain.substring(document.domain.indexOf(".oauth.reddit.com") + 1) == "oauth.reddit.com") || (document.domain == "pir.reddit.com" || document.domain.substring(document.domain.indexOf(".pir.reddit.com") + 1) == "pir.reddit.com") || (document.domain == "pl.reddit.com" || document.domain.substring(document.domain.indexOf(".pl.reddit.com") + 1) == "pl.reddit.com") || (document.domain == "pt.reddit.com" || document.domain.substring(document.domain.indexOf(".pt.reddit.com") + 1) == "pt.reddit.com") || (document.domain == "pt-pt.reddit.com" || document.domain.substring(document.domain.indexOf(".pt-pt.reddit.com") + 1) == "pt-pt.reddit.com") || (document.domain == "pt_BR.reddit.com" || document.domain.substring(document.domain.indexOf(".pt_BR.reddit.com") + 1) == "pt_BR.reddit.com") || (document.domain == "ro.reddit.com" || document.domain.substring(document.domain.indexOf(".ro.reddit.com") + 1) == "ro.reddit.com") || (document.domain == "ru.reddit.com" || document.domain.substring(document.domain.indexOf(".ru.reddit.com") + 1) == "ru.reddit.com") || (document.domain == "sk.reddit.com" || document.domain.substring(document.domain.indexOf(".sk.reddit.com") + 1) == "sk.reddit.com") || (document.domain == "sr.reddit.com" || document.domain.substring(document.domain.indexOf(".sr.reddit.com") + 1) == "sr.reddit.com") || (document.domain == "sr-la.reddit.com" || document.domain.substring(document.domain.indexOf(".sr-la.reddit.com") + 1) == "sr-la.reddit.com") || (document.domain == "sv.reddit.com" || document.domain.substring(document.domain.indexOf(".sv.reddit.com") + 1) == "sv.reddit.com") || (document.domain == "ta.reddit.com" || document.domain.substring(document.domain.indexOf(".ta.reddit.com") + 1) == "ta.reddit.com") || (document.domain == "th.reddit.com" || document.domain.substring(document.domain.indexOf(".th.reddit.com") + 1) == "th.reddit.com") || (document.domain == "tr.reddit.com" || document.domain.substring(document.domain.indexOf(".tr.reddit.com") + 1) == "tr.reddit.com") || (document.domain == "uk.reddit.com" || document.domain.substring(document.domain.indexOf(".uk.reddit.com") + 1) == "uk.reddit.com") || (document.domain == "us-en.reddit.com" || document.domain.substring(document.domain.indexOf(".us-en.reddit.com") + 1) == "us-en.reddit.com") || (document.domain == "us.reddit.com" || document.domain.substring(document.domain.indexOf(".us.reddit.com") + 1) == "us.reddit.com") || (document.domain == "vi.reddit.com" || document.domain.substring(document.domain.indexOf(".vi.reddit.com") + 1) == "vi.reddit.com") || (document.domain == "zh.reddit.com" || document.domain.substring(document.domain.indexOf(".zh.reddit.com") + 1) == "zh.reddit.com") || (document.domain == "zh.cn.reddit.com" || document.domain.substring(document.domain.indexOf(".zh.cn.reddit.com") + 1) == "zh.cn.reddit.com") || (document.domain == "translate.google.com" || document.domain.substring(document.domain.indexOf(".translate.google.com") + 1) == "translate.google.com") || (document.domain == "translate.googleusercontent.com" || document.domain.substring(document.domain.indexOf(".translate.googleusercontent.com") + 1) == "translate.googleusercontent.com"))
	css += [
		"/* ",
		"/* Cursor - Plain Black - KMV",
		"/* taken from:",
		"/* https://userstyles.org/styles/175086/",
		"/*",
		"/* Other Themes by KMV:",
		"/* https://userstyles.org/users/370722",
		"/*",
		"/* Contact - https://www.reddit.com/r/KMV/",
		"*/",
		"html,",
		"body,",
		"div,",
		"element,",
		"input[type=\"submit\"],",
		"label,",
		"select,",
		"/*span,*/",
		"svg,",
		"input#rem-login-main,",
		"._3YDPJHFl8YQG4TIAGQwHwK,",
		"._1HVKaY4Y3yukzOicpaGXLB,",
		"._3-_K1uspRApIM1_dkDMKnM",
		"a:hover,",
		"a:hover *,",
		"li a:hover,",
		"button:hover,",
		"input#file,",
		"th:hover,",
		"use:hover,",
		".arrow:hover,",
		".arrow-upvote:hover,",
		".arrow-downvote:hover,",
		".blueButton,",
		".css_button:hover,",
		".dismiss:hover,",
		".expando-button:hover,",
		".gearIcon:hover,",
		".md-spoiler-text,",
		".md-spoiler-text p,",
		".radio-response:hover,",
		".redditSingleClick::after,",
		".RESCloseButton:hover,",
		".RESDropdownList li:hover,",
		".res-step:hover,",
		".toggleButton .toggleThumb,",
		".WnMeTcero48dKo501T-19:hover,",
		"._1B7SUGHK0QDoHimxX2a9D0:hover,",
		"._1FE1o9C7Gk8k6ciF8-oeXx,",
		"._1rNBkuuOkN2SorEXyRkYjB,",
		"._1QLRzzdhJM_LEu2w38yEOx,",
		"._1RYN-7H8gYctjOQeL8p2Q7,",
		"._23h0-EcaBUorIHC-JZyh6J,",
		"._28vEaVlLWeas1CDiLuTCap,",
		"._2ZjElFi3ORaU3VPrwmdoCp:hover,",
		"._2ED-O3JtIcOqp8iIL1G5cg,",
		"._2FCtq-QzlfuN-SwVMUZMM3:hover,",
		"._2XDITKxlj4y3M99thqyCsO,",
		"._2XQ3ZY6qCbEm9_WtvLLFru,",
		"._2y3bja4n4-unxyUrMEFH8C,",
		"._3-Dc7BBLD7JWsyF3pV-rsH,",
		"._3-miAEojrCvx_4FQ8x3P-s,",
		"._36kpXQ-z7Hr61j8878uRkP,",
		"._3bzYm5d0EW5ZCDVM6kmYIh,",
		"._3G7xHJZQMrQlpjhNDQI2fe,",
		"._3UAl61hrkRAXX5JQ6m_q8R:hover,",
		"._3u6g9UTYlEOr-yfM5hyq3p,",
		"._1XWObl-3b9tPy64oaG6fax,",
		".cZPZhMe-UCZ8htPodMyJ5,",
		".D3IyhBGwXo9jPwz-Ka0Ve._1poyrkZ7g36PawDueRza-J,",
		".Nb7NCPTlQuxN_WDPUg5Q2:hover,",
		".Ox8QhMwI_pG4DcRAicFqa:hover,",
		".ssgs3QQidkqeycI33hlBa,",
		".TmgZY6tDcdErbE5d7E0HJ,",
		".Ywkt6EDfNWINeTr9lP29H:hover",
		"li a:active,",
		"button:active,",
		"input#file:active,",
		"th:active,",
		"use:active,",
		".arrow:active,",
		".arrow-upvote:active,",
		".arrow-downvote:active,",
		".css_button:active,",
		".dismiss:active,",
		".expando-button:active,",
		".gearIcon:active,",
		".md-spoiler-text:active,",
		".md-spoiler-text p:active,",
		".no-select:active,",
		".radio-response:active,",
		".RESCloseButton:active,",
		".RESDropdownList li:active,",
		".res-step:active,",
		".toggleButton .toggleThumb:active,",
		"._1B7SUGHK0QDoHimxX2a9D0:active,",
		"._1rNBkuuOkN2SorEXyRkYjB:active,",
		"._1RYN-7H8gYctjOQeL8p2Q7:active,",
		"._1XWObl-3b9tPy64oaG6fax:active,",
		"._23h0-EcaBUorIHC-JZyh6J:active,",
		"._28vEaVlLWeas1CDiLuTCap:active,",
		"._2ED-O3JtIcOqp8iIL1G5cg:active,",
		"._2FCtq-QzlfuN-SwVMUZMM3:active,",
		"._2uVDwsKlmWPhYjwe_hYwKZ:active,",
		"._2XDITKxlj4y3M99thqyCsO:active,",
		"._2XQ3ZY6qCbEm9_WtvLLFru:active,",
		"._2y3bja4n4-unxyUrMEFH8C:active,",
		"._2ZjElFi3ORaU3VPrwmdoCp:active,",
		"._3-Dc7BBLD7JWsyF3pV-rsH:active,",
		"._3-miAEojrCvx_4FQ8x3P-s:active,",
		"._36kpXQ-z7Hr61j8878uRkP:active,",
		"._3G7xHJZQMrQlpjhNDQI2fe:active,",
		"._3u6g9UTYlEOr-yfM5hyq3p:active,",
		"._3UAl61hrkRAXX5JQ6m_q8R:active,",
		"._3UEq__yL-82zX4EyuluREz:active,",
		".Ox8QhMwI_pG4DcRAicFqa:active,",
		".cZPZhMe-UCZ8htPodMyJ5:active,",
		".D3IyhBGwXo9jPwz-Ka0Ve._1poyrkZ7g36PawDueRza-J:active,",
		".Nb7NCPTlQuxN_WDPUg5Q2:active,",
		".ssgs3QQidkqeycI33hlBa:active,",
		".TmgZY6tDcdErbE5d7E0HJ:active,",
		".Ywkt6EDfNWINeTr9lP29H:active",
	].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();
