// ==UserScript==
// @name          Washington Post AdBlocker Blocker Remover
// @namespace     http://userstyles.org
// @description	  Removes the screen that shows up on wapo if you have an AdBlocker
// @author        afxjzs
// @homepage      https://userstyles.org/styles/149142
// @include       http://www.washingtonpost.com/*
// @include       https://www.washingtonpost.com/*
// @include       http://*.www.washingtonpost.com/*
// @include       https://*.www.washingtonpost.com/*
// @run-at        document-start
// @version       0.20171004174922
// ==/UserScript==
(function() {var css = [
	"html.drawbridge-up,",
	"html.drawbridge-up #pb-root,",
	"html.drawbridge-up body {",
	"    overflow: scroll;",
	"}",
	"#bottom-furniture .moat-trackable {",
	"    display: none;",
	"}"
].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();
